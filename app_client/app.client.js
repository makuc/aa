(function() {
    /* global angular */
    angular.module('aa-novels', ['ngRoute', 'ngSanitize', 'ui.bootstrap']);
    
    function configuration($routeProvider, $locationProvider) {
        $routeProvider
            /* User management*/
            .when('/register', {
                templateUrl: 'views/registration.template.html',
                controller: 'register.controller',
                controllerAs: 'vm'
            })
            .when('/login', {
                templateUrl: 'views/login.template.html',
                controller: 'login.controller',
                controllerAs: 'vm'
            })
            
            /* Novels lists */
            .when('/', {
                templateUrl: 'views/browse.template.html',
                controller: 'browse.controller',
                controllerAs: 'vm' // vm = view-model
            })
            .when('/browse', {
                templateUrl: 'views/browse.template.html',
                controller: 'browse.controller',
                controllerAs: 'vm' // vm = view-model
            })
            .when('/browse', {
                templateUrl: 'views/browse.template.html',
                controller: 'browse.controller',
                controllerAs: 'vm'
            })
            .when('/library', {
                templateUrl: 'views/library.template.html',
                controller: 'library.controller',
                controllerAs: 'vm'
            })
            .when('/mynovels', {
                templateUrl: 'views/mynovels.template.html',
                controller: 'mynovels.controller',
                controllerAs: 'vm'
            })
            .when('/search', {
                templateUrl: 'views/browse.template.html',
                controller: 'search.controller',
                controllerAs: 'vm'
            })
            
            /* Novels management */
            .when('/novel', {
                templateUrl: 'views/novel-edit.template.html',
                controller: 'novelEdit.controller',
                controllerAs: 'vm'
            })
            .when('/novel/:novel_id', {
                templateUrl: 'views/novel.template.html',
                controller: 'novel.controller',
                controllerAs: 'vm'
            })
            .when('/novel/:novel_id/edit', {
                templateUrl: 'views/novel-edit.template.html',
                controller: 'novelEdit.controller',
                controllerAs: 'vm'
            })
            .when('/novel/:novel_id/c', {
                templateUrl: '/views/chapter-edit.template.html',
                controller: 'chapterEdit.controller',
                controllerAs: 'vm'
            })
            .when('/novel/:novel_id/c/:chapter_id', {
                templateUrl: '/views/chapter.template.html',
                controller: 'chapter.controller',
                controllerAs: 'vm'
            })
            .when('/novel/:novel_id/c/:chapter_id/edit', {
                templateUrl: '/views/chapter-edit.template.html',
                controller: 'chapterEdit.controller',
                controllerAs: 'vm'
            })
            
            /* Other pages */
            .when('/about', {
                templateUrl: 'views/about.template.html',
                controller: 'about.controller',
                controllerAs: 'vm'
            })
            .when('/403', {
                templateUrl: 'views/about.template.html',
                controller: '403.controller',
                controllerAs: 'vm'
            })
            .when('/db', {
                templateUrl: 'views/db.template.html',
                controller: 'db.controller',
                controllerAs: 'vm'
            })
            .otherwise({redirectTo: '/'});
        $locationProvider.html5Mode(true);
    }
    
    angular
        .module('aa-novels')
        .config(['$routeProvider', '$locationProvider', configuration])
        
        // Register interceptors
        .factory('httpRequestInterceptor', httpInterceptor)
        .config(httpInterceptorConfig);
    
    function httpInterceptorConfig($httpProvider) {
        $httpProvider.interceptors.push('httpRequestInterceptor');
    }
    httpInterceptorConfig.$inject = ['$httpProvider'];
    function httpInterceptor($window) {
        return {
            request: function (config) {
                config.headers['x-access-token'] = $window.localStorage['aa-novels-token'];
                return config;
            }
        };
    }
    httpInterceptor.$inject = ['$window'];
})();