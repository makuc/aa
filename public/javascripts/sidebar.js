String.prototype.splice = function(i, str) {
  var first = this.substring(0, i);
  var last = this.substring(i, this.length);
  
  return first + str + last;
};
String.prototype.wrap = function(i1, i2, ele) {
  var first = this.substring(0, i1);
  var middle = this.substring(i1, i2);
  var end = this.substring(i2, this.length);
  return first + "<" + ele + ">" + middle + "</" + ele + ">" + end;
};

/* global $ */
$(document).ready(function () {
  
  //Sidebar move
  var onResize = function() {
    // apply dynamic padding at the top of the body according to the fixed navbar height
    $("#sidebar").css("top", $(".navbar-fixed-top").height());
    $("#chapters").css("top", $(".navbar-fixed-top").height());
  };
  // attach the function to the window resize event
  $(window).resize(onResize);
  // call it also when the page is ready after load or reload
  $(function() {
    onResize();
  });
  
	// --> pasteHtmlAtCaret("div", "<b>INSERTED</b>");
	function pasteHtmlAtCaret(el, html) {
    var selection, range;
    if (window.getSelection) {
      
      // IE9 and non-IE
      
      selection = window.getSelection();
      
      if (selection.getRangeAt && selection.rangeCount) {
        range = selection.getRangeAt(0);
        range.deleteContents();

        // Range.createContextualFragment() would be useful here but is
        // non-standard and not supported in all browsers (IE9, for one)
        
        // Creates container and fills it with data
        var el = document.createElement(el);
        el.innerHTML = html;
        
        var frag = document.createDocumentFragment(), node, lastNode;
        while ( (node = el.firstChild) ) {
          lastNode = frag.appendChild(node);
        }
        
        var rangeClone = range.cloneRange();
        
        var selectedContent = range.cloneContents();
        
        range.insertNode(frag);
        
        // Preserve the selection
        if (lastNode) {
          range = range.cloneRange();
          range.setStartAfter(lastNode);
          range.collapse(true);
          selection.removeAllRanges();
          selection.addRange(range);
        }
      }
    } else if (document.selection && document.selection.type != "Control") {
      // IE < 9
      document.selection.createRange().pasteHTML(html);
    }
  }
	
	
	// Bold selected text
  $('#font-bold').on('click', function(e) {
    e.preventDefault();
    var ed = window.getSelection();
    
    var f = ed.anchorNode.parentNode;
    var l = ed.focusNode.parentNode;
    
    if(f == l) {
      f.innerHTML = f.innerHTML.wrap(ed.anchorOffset, ed.focusOffset, "b");
    }
  });
  $('#font-italic').on('click', function(e) {
    e.preventDefault();
    var ed = window.getSelection();
    
    var f = ed.anchorNode.parentNode;
    var l = ed.focusNode.parentNode;
    
    if(f == l) {
      f.innerHTML = f.innerHTML.wrap(ed.anchorOffset, ed.focusOffset, "i");
    }
  });
});

