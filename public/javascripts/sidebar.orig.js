String.prototype.splice = function(i, str) {
  var first = this.substring(0, i);
  var last = this.substring(i, this.length);
  
  return first + str + last;
};
String.prototype.wrap = function(i1, i2, ele) {
  var first = this.substring(0, i1);
  var middle = this.substring(i1, i2);
  var end = this.substring(i2, this.length);
  return first + "<" + ele + ">" + middle + "</" + ele + ">" + end;
};

/* global $, localStorage */
$(document).ready(function () {
  // Tooltips
  $('.tooltip-enable').tooltip({
    trigger : 'hover'
  });
  
  // Control Chapters display
  $('#chapters-toggle').on('click', function(e) {
    e.preventDefault();
    $('#chapters').toggleClass('collapse');
  });
  
  // Sidebar controls
  if(localStorage.getItem("sidebar") == "open") {
    $('#sidebar').toggleClass('collapse');
    $('#sidebar-open').toggleClass('collapse');
  }
  $('#sidebar-open').on('click', function (e) {
    e.preventDefault();
    $('#sidebar').toggleClass('collapse');
    $('#sidebar-open').toggleClass('collapse');
    localStorage.setItem("sidebar", "open");
  });
  $('#sidebar-close').on('click', function (e) {
    e.preventDefault();
    $('#sidebar').toggleClass('collapse');
    $('#sidebar-open').toggleClass('collapse');
    $('#chapters').addClass('collapse');
    localStorage.setItem("sidebar", "closed");
  });
  
  // Chapter display settings
  if(localStorage.getItem("fontSize") !== null) {
    $('body').css('font-size', localStorage.getItem('fontSize') + "px");
  }
  $('#font-enlarge').on('click', function(e) {
    e.preventDefault();
    resizeFont(true);
  });
  $('#font-shrink').on('click', function(e) {
    e.preventDefault();
    resizeFont(false);
  })
  
  var resizeFont = function(enlarge) {
    var fontSize;
    if( localStorage.getItem('fontSize') !== null ) {
      var fontSize = localStorage.getItem('fontSize');
    } else {
      fontSize = parseInt($('body').css('font-size').replace('px', ''));
    }
    if(enlarge) {
      ++fontSize;
    } else {
      if(fontSize -1 >= 9) {
        --fontSize;
      }
    }
    localStorage.setItem("fontSize", fontSize);
    $('body').css('font-size', fontSize + "px");
  }
  
  // Change Day/Night mode od view
  var dayNight = function(change) {
    if(localStorage.getItem("displayMode") === null) {
      localStorage.setItem("displayMode", "day");
    }
    if(change) {
      if(localStorage.getItem('displayMode') == "day") {
        localStorage.setItem('displayMode', "night");
      } else {
        localStorage.setItem('displayMode', 'day');
      }
    }
    if(localStorage.getItem('displayMode') == "day") {
      $('.navbar-inverse').addClass('navbar-default').removeClass('navbar-inverse');
      $('body').removeClass('nighttime');
    } else {
      $('.navbar-default').addClass('navbar-inverse').removeClass('navbar-default');
      $('body').addClass('nighttime');
    }
  };
  $('#displayMode').on('click', function(e) {
    e.preventDefault();
    dayNight(true);
  });
  if(localStorage.getItem("displayMode") !== null) {
    dayNight(false);
  }
  
  //Sidebar move
  var onResize = function() {
    // apply dynamic padding at the top of the body according to the fixed navbar height
    $("#sidebar").css("top", $(".navbar-fixed-top").height());
    $("#chapters").css("top", $(".navbar-fixed-top").height());
  };
  // attach the function to the window resize event
  $(window).resize(onResize);
  // call it also when the page is ready after load or reload
  $(function() {
    onResize();
  });
  
  // Delete entry
  $('#delete').on('click', function(e) {
    e.preventDefault();
    var url = window.location.href;
    url = url.replace(/^http.:\/\//i, '');
    url = url.split(/[/?&=#]/);
    
    //console.log(url);
    
    var form = document.createElement("form");
    form.style.display = "none";
    document.body.appendChild(form);
    form.method = "POST";
    
    if(url[3] == "edit") {
      form.action = "/" + url[1] + "/" + url[2] + "/delete";
    } else if (url[4] == "edit") {
      form.action = "/" + url[1] + "/" + url[2] + "/" + url[3] + "/delete";
    }
    
    var elAuthor = document.createElement("input");
    elAuthor.name = "author";
    form.appendChild(elAuthor);
    elAuthor.value = localStorage.getItem("token");
    
    form.submit();
    
  });
  // Save changes: Novel
  $('#save').on('click', function(e) {
    e.preventDefault();
    var url = window.location.href;
    url = url.replace(/^http[s]*:\/\//i, '');
    url = url.split(/[/?#]/);
    
    //console.log(url);
    
    var tags, title, dscr, chapter;
    var form, elTitle, elDscr, elTags, elChapter;
    if(url[1] == "novel" && url[2] == undefined | "")
    {// Add novel
      
      tags = $('#tags').contents().text();
      title = $('#novelTitle').contents().text();
      dscr = $('#details-description').html();
      
      form = document.createElement("form");
      form.style.display = "none";
      document.body.appendChild(form);
      form.method = "POST";
      form.action = "/novel";
      elTitle = document.createElement("input");
      form.appendChild(elTitle);
      elDscr = document.createElement("input");
      form.appendChild(elDscr);
      elTags = document.createElement("input");
      form.appendChild(elTags);
      
      elTitle.name = "title";
      elTitle.value = title;
      elDscr.name = "description";
      elDscr.value = dscr;
      elTags.name = "tags";
      elTags.value = tags;
      
      form.submit();
    } else if(url[3] == "edit")
    {// Edit novel
      
      tags = $('#tags').contents().text();
      title = $('#novelTitle').contents().text();
      dscr = $('#details-description').html();
      
      form = document.createElement("form");
      form.style.display = "none";
      document.body.appendChild(form);
      form.method = "POST";
      form.action = "/" + url[1] + "/" + url[2] + "/edit";
      elTitle = document.createElement("input");
      form.appendChild(elTitle);
      elDscr = document.createElement("input");
      form.appendChild(elDscr);
      elTags = document.createElement("input");
      form.appendChild(elTags);
      
      elTitle.name = "title";
      elTitle.value = title;
      elDscr.name = "description";
      elDscr.value = dscr;
      elTags.name = "tags";
      elTags.value = tags;
      
      form.submit();
      /*
      // Update tags properly into Tags container
      tags = tags.split(/[,;]/);
      
      var tagsPanel = $('#tags-panel');
      tagsPanel.empty();
      
      tags.forEach(function(tag) {
        tag = tag.trim();
        if(tag !== "") {
          tagsPanel.append("<span class=\"label label-info\"><span class=\"glyphicon glyphicon-ok\"></span>" + tag + "</span>");
        }
      });
      */
    } else if(url[3] == "c" && url[4] == undefined || "")
    {// Add chapter
      
      title = $('#chapter-title').contents().text();
      chapter = $('#chapter-content').html().replace(/\n/g, '</p><p>');
      
      form = document.createElement("form");
      form.style.display = "none";
      form.method = "POST";
      form.action = "/novel/" + url[2] + "/c";
      
      document.body.appendChild(form);
      
      elTitle = document.createElement("input");
      form.appendChild(elTitle);
      elChapter = document.createElement("input");
      form.appendChild(elChapter);
      
      elTitle.name = "title";
      elTitle.value = title;
      elChapter.name = "chapter";
      elChapter.value = chapter;
      
      form.submit();
      
    } else if(url[3] == "c" && url[5] == "edit")
    {// Edit chapter
      
      title = $('#chapter-title').contents().text();
      chapter = $('#chapter-content').html();
      
      form = document.createElement("form");
      form.style.display = "none";
      form.method = "POST";
      form.action = "/novel/" + url[2] + "/c/" + url[4] + "/edit";
      
      document.body.appendChild(form);
      
      elTitle = document.createElement("input");
      form.appendChild(elTitle);
      elChapter = document.createElement("input");
      form.appendChild(elChapter);
      
      elTitle.name = "title";
      elTitle.value = title;
      elChapter.name = "chapter";
      elChapter.value = chapter;
      
      form.submit();
    }
  });
  
	// --> pasteHtmlAtCaret("div", "<b>INSERTED</b>");
	function pasteHtmlAtCaret(el, html) {
    var selection, range;
    if (window.getSelection) {
      
      // IE9 and non-IE
      
      selection = window.getSelection();
      
      if (selection.getRangeAt && selection.rangeCount) {
        range = selection.getRangeAt(0);
        range.deleteContents();

        // Range.createContextualFragment() would be useful here but is
        // non-standard and not supported in all browsers (IE9, for one)
        
        // Creates container and fills it with data
        var el = document.createElement(el);
        el.innerHTML = html;
        
        var frag = document.createDocumentFragment(), node, lastNode;
        while ( (node = el.firstChild) ) {
          lastNode = frag.appendChild(node);
        }
        
        var rangeClone = range.cloneRange();
        
        var selectedContent = range.cloneContents();
        
        range.insertNode(frag);
        
        // Preserve the selection
        if (lastNode) {
          range = range.cloneRange();
          range.setStartAfter(lastNode);
          range.collapse(true);
          selection.removeAllRanges();
          selection.addRange(range);
        }
      }
    } else if (document.selection && document.selection.type != "Control") {
      // IE < 9
      document.selection.createRange().pasteHTML(html);
    }
  }
	
	
	// Bold selected text
  $('#font-bold').on('click', function(e) {
    e.preventDefault();
    var ed = window.getSelection();
    
    var f = ed.anchorNode.parentNode;
    var l = ed.focusNode.parentNode;
    
    if(f == l) {
      f.innerHTML = f.innerHTML.wrap(ed.anchorOffset, ed.focusOffset, "b");
    }
  });
  $('#font-italic').on('click', function(e) {
    e.preventDefault();
    var ed = window.getSelection();
    
    var f = ed.anchorNode.parentNode;
    var l = ed.focusNode.parentNode;
    
    if(f == l) {
      f.innerHTML = f.innerHTML.wrap(ed.anchorOffset, ed.focusOffset, "i");
    }
  });
});

