/* global $, localStorage */
// Write review - check required data
$('#add-review').click(function(dogodek) {
  $('.alert.alert-danger').hide();
  if (!$('select#ocena').val()) {
    if ($('.alert.alert-danger').length) {
      $('.alert.alert-danger').show();
    } else {
      $(this).parent().prepend('<div role="alert" class="alert alert-danger">Prosim, izpolnite vsa vnosna polja!</div>');
      dogodek.preventDefault();
    }
    return false;
  }
});

// If already logged in, redirect
$('#login').ready(function() {
  var url = window.location.href;
  url = url.replace(/^http.:\/\//i, '');
  url = url.split(/[/?&=#]/);
  
  if(url[2] == "login" || url[2] == "register") {
    var token = localStorage.getItem("token");
    console.log(token);
    if(token !== undefined && token !== null) {
      window.location.href = "/library";
    }
  } else if(url[2] == "signout") {
    localStorage.removeItem("token");
  }
});
// Login data validation
$("#btn-login").click(function(event) {
	var email = $("#email").val();
	var password = $("#pwd").val();
	
	var errorMsg = $('#error-msg');
	errorMsg.empty();
	
	if(email == "") {
	  event.preventDefault();
	  $("#errorMessage").removeClass("hidden");
	  errorMsg.append("Email can't be empty!");
	} else if(!isEmail(email)){
	  event.preventDefault();
	  $("#errorMessage").removeClass("hidden");
	  errorMsg.append("This isn't a valid email address.");
	} else if(password == "") {
	  event.preventDefault();
	  $("#errorMessage").removeClass("hidden");
	  errorMsg.append("Password can't be empty!");
	}
});

// Register Form Validation
$("#btn-register").click(function(event) {
  var creatorName = $('#creator-name').val();
	var email = $("#email").val();
	var password = $("#pwd").val();
	var passwordR = $('#pwd-repeat').val();
	var terms = $('#terms').is(":checked");
	
	var errorMsg = $('#error-msg');
	errorMsg.empty();
	
	var err = 0;
	
	if(creatorName == "") {
	  errorMsg.append("<p>Your Creator's name is required</p>");
	  $('#creator-name').parent().parent('.form-group').addClass('has-error');
	  
	  err++;
	} else {
	  $('#creator-name').parent().parent().removeClass('has-error');
	}
	if(email == "") {
	  errorMsg.append("<p>Your email is require</p>");
	  $('#email').parent().parent().addClass('has-error');
	  
	  err++;
	} else if(!isEmail(email)){
	  errorMsg.append("<p>This isn't a valid email address.</p>");
	  $('#email').parent().parent().addClass('has-error');
	  
	  err++;
	} else {
	  $('#email').parent().parent().removeClass('has-error');
	}
	if(password == "") {
	  errorMsg.append("<p>Password is required</p>");
	  $('#pwd').parent().parent().addClass('has-error');
	  
	  err++;
	} else {
	  $('#pwd').parent().parent().removeClass('has-error');
	}
	if(passwordR == "") {
	  errorMsg.append("<p>A repeated password is required, to make sure you meant to set it as such.</p>");
	  $('#pwd-repeat').parent().parent().addClass('has-error');
	  
	  err++;
	}
	if(password != "" && passwordR != "" && password != passwordR) {
	  errorMsg.append("<p>Paswords don't match");
	  $('#pwd').parent().parent().addClass('has-error');
	  $('#pwd-repeat').parent().parent().addClass('has-error');
	  
	  err++;
	} else if(password != "" && password == passwordR){
	  $('#pwd').parent().parent().removeClass('has-error');
	  $('#pwd-repeat').parent().parent().removeClass('has-error');
	}
	if(!terms) {
	  errorMsg.append("<p>You have to agree to the terms of use");
	  $('#terms').parent().parent().addClass('has-error');
	  
	  err++;
	} else {
	  $('#terms').parent().parent().removeClass('has-error');
	}
	
	if(err > 0){
	  event.preventDefault();
	  $("#errorMessage").removeClass("hidden");
	} else {
	  $("#errorMessage").addClass("hidden");
	}
});

// Check if inputted string is formatted as a proper email
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
