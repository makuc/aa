/* global $, localStorage */
$(document).ready(function () {
    // Save genres checked states to localStorage
    var genreStates = JSON.parse(localStorage.getItem('genreStates')) || {};
    var $genres = $("#genres :checkbox");
    
    $genres.on("change", function(){
        $genres.each(function(){
            genreStates[this.id] = this.checked;
        });
        localStorage.setItem("genreStates", JSON.stringify(genreStates));
    });
    // Update genres states from localStorage !!
    $.each(genreStates, function(key, value) {
        $("#" + key).prop('checked', value);
    });
});
