var express = require('express');
var router = express.Router();
var controllers = {
    users: require("../controllers/users.controllers"),
    novels: require('../controllers/novels.controllers'),
    reviews: require('../controllers/reviews.controllers'),
    chapters: require("../controllers/chapters.controllers"),
    tags: require("../controllers/tags.controllers")
};

// Authentication
router.get("/logout", controllers.users.logout);

// User-Novel
router.get("/my", controllers.novels.getMyNovels);
router.get("/library", controllers.users.getLibrary);

// Novels
router.post('/novels', controllers.novels.addNovel);
router.put('/novels/:novel_id', controllers.novels.updateNovel);
router.delete('/novels/:novel_id', controllers.novels.deleteNovel);

// Chapters
router.post('/novels/:novel_id', controllers.chapters.addChapter);
router.put('/novels/:novel_id/:chapter_id', controllers.chapters.updateChapter);
router.delete('/novels/:novel_id/:chapter_id', controllers.chapters.deleteChapter);

// Reviews
router.post('/novels/:novel_id/review', controllers.reviews.addReview);
router.put('/novels/:novel_id/review/:review_id', controllers.reviews.updateReview);
router.delete('/novels/:novel_id/review/:review_id', controllers.reviews.deleteReview);

// Users
router.get("/users", controllers.users.getUsers);
router.get("/users/me", controllers.users.getUserMe);
router.post("/users/library", controllers.users.addToLibrary);
router.delete("/users/library", controllers.users.removeFromLibrary);
router.delete("/users/library/:novel_id", controllers.users.removeFromLibrary2);
router.get("/users/:user_id", controllers.users.getUser);
router.put("/users", controllers.users.updateUser);
router.delete("/users", controllers.users.deleteUser);



module.exports = router;