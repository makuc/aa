var express = require('express');
var router = express.Router();
var controllers = {
    other: require("../controllers/other.controllers"),
    users: require("../controllers/users.controllers"),
    novels: require('../controllers/novels.controllers'),
    reviews: require('../controllers/reviews.controllers'),
    chapters: require("../controllers/chapters.controllers"),
    db: require('../controllers/db.controllers'),
    tags: require("../controllers/tags.controllers")
};

router.get("/", controllers.other.welcome);

// Novels
router.get('/novels', controllers.novels.getNovels);
router.get('/novels/:novel_id', controllers.novels.getNovel);
// Chapters
router.get('/novels/:novel_id/:chapter_id', controllers.chapters.getChapter);
// Reviews
router.get('/novels/:novel_id/review/:review_id', controllers.reviews.getReview);
// Users
router.post("/login", controllers.users.login);
router.post("/users", controllers.users.addUser);
// Tags
router.get("/tags", controllers.tags.getTags);
// Db
router.delete('/db', controllers.db.drop);
router.post('/db', controllers.db.populate);

module.exports = router;