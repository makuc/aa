var jwt = require("jsonwebtoken");

module.exports.private = function(req, res, next) {
    var url = req.originalUrl;
    url = url.split(/[\/?#]/g);
    
    if(url[1] == "api") {
        // Check if user has been authenticated
        if (!req.user) 
            return res.status(403).send({ auth: false, message: 'No valid token provided.' });
        next();
    } else
        next();
};
module.exports.authenticate = function(req, res, next) {
    // check header or url parameters or post parameters for token
    
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    
    if(token) {
        // verifies secret and checks exp
        jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {      
            if (err) {
                /*
                if(req.headers['static-page'] !== 'true')
                    console.log("UserID: Guest with invalid token | Url: " + req.originalUrl);
                */
                return next();
            }  
            
            // if everything is good, save to request for use in other routes
            req.user = decoded;
            /*
            if(req.headers['static-page'] !== 'true')
                console.log("UserID: " + decoded._id + " | Email: " + decoded.email + " | Url: " + req.originalUrl);
            */
            next();
        });
    } else {
        /*
        if(req.headers['static-page'] !== 'true')
            console.log("UserID: Guest | Url: " + req.originalUrl);
        */
        next();
    }
};