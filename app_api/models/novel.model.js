var mongoose = require('mongoose');

var reviewsSchema = new mongoose.Schema({
    author: {type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User'},
    rating: {type: Number, required: true, min: 1, max: 5},
    date: {type: Date, "default": Date.now},
    review: String
}, { _id: true });

var novelSchema = new mongoose.Schema({
    title: {type: String, required: true},
    author: {type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User'},
    description: {type: String, "default": "No description"},
    rating: {type: Number, "default": 0, min: 0, max: 5},
    tags: [String],
    chapters: [{type: mongoose.Schema.Types.ObjectId, ref: 'Chapter'}],
    reviews: [reviewsSchema]
});

// Save this Scheme as a model
mongoose.model('Novel', novelSchema, 'Novels');
