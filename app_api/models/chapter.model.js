var mongoose = require('mongoose');

var chapterSchema = new mongoose.Schema({
    novel: {type: mongoose.Schema.Types.ObjectId, ref: 'Novel', required: true},
    title: String,
    chapter: String,
    date: {type: Date, "default": Date.now},
    lastEdit: {type: Date, "default": null}
});

// Save this Scheme as a model
mongoose.model('Chapter', chapterSchema, 'Chapters');