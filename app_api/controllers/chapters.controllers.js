var mongoose = require('mongoose');
var Novel = mongoose.model('Novel');
var Chapter = mongoose.model('Chapter');

// Managing chapters
module.exports.addChapter = function(req, res) {
    Novel
        .findById(req.params.novel_id)
        .select('author chapters')
        .exec(function(err, novel) {
            if(err) {
                //console.log(err);
                return res.status(404).send({ message: "Requested novel cannot be found, hence chapter cannot be added" });
            }
            if(!novel) return res.status(404).send({ message: "Requested novel cannot be found, hence chapter cannot be added" });
            
            // Check if user is the author
            if(req.user._id != novel.author)
                return res.status(403).send({ message: "You don't have permission for adding chapters to this novel" });
            
            addChapter(req, res, novel);
        });
};
module.exports.getChapter = function(req, res) {
    Chapter
        .findOne({
            novel: req.params.novel_id,
            _id: req.params.chapter_id
        })
        .populate([
            {
                path: "novel",
                select: "_id title author chapters",
                populate: [
                    {
                        path: "chapters",
                        select: "_id title"
                    },
                    {
                        path: "author",
                        select: "_id name"
                    }
                ]
            }
        ])
        .select("-__v")
        .exec(function(err, chapter) {
            if(err) {
                //console.log(err);
                return res.status(404).send({ message: "Requested chapter cannot be found" });
            }
            if(!chapter) return res.status(404).send({ message: "Requested chapter cannot be found for this novel" });
            
            res.status(200).send(chapter);
        });
};
module.exports.updateChapter = function(req, res) {
    Chapter
        .findOne({ novel: req.params.novel_id, _id: req.params.chapter_id })
        .select("-__v")
        .populate("novel", "author")
        .exec(function(err, chapter) {
            if(err) {
                //console.log(err);
                return res.status(404).send({ message: "Requested chapter cannot be found" });
            }
            if(!chapter) return res.status(404).send({ message: "Requested chapter cannot be found for this novel" });
            
            // Check permissions for updating
            if(chapter.novel.author != req.user._id)
                return res.status(403).send({ message: "You don't have permission to update this chapter" });
            
            if(req.body.title)
                chapter.title = validateString(req.body.title);
            if(req.body.chapter)
                chapter.chapter = validateString(req.body.chapter);
            chapter.lastEdit = Date.now();
            
            chapter.save(function(err, chapter) {
                if(err) {
                    //console.log(err);
                    return res.status(400).send({ message: "Something went wrong while saving the chapter. Please try again." });
                }
                
                res.status(200).send(chapter);
            });
        });
};
module.exports.deleteChapter = function(req, res) {
    Chapter
        .findOne({ novel: req.params.novel_id, _id: req.params.chapter_id })
        .populate("novel", "author")
        .exec(function(err, chapter) {
            if(err) {
                //console.log(err);
                return res.status(404).send({ message: "Requested chapter cannot be found" });
            }
            if(!chapter) return res.status(404).send({ message: "Requested chapter cannot be found for this novel" });
            
            // Check permissions for this user
            if(chapter.novel.author != req.user._id)
                return res.status(403).send({ message: "You don't have permission to delete this chapter" });
            
            Novel
                .findById(chapter.populated("novel"))
                .exec(function(err, novel) {
                    if(err) {
                        //console.log(err);
                        return res.status(404).send({ message: "Requested novel cannot be found" });
                    }
                    if(!novel) return res.status(404).send({ message: "Requested novel cannot be found" });
                    
                    novel.chapters.remove(chapter._id);
                    
                    chapter.remove();
                    
                    novel.save(function(err, novel) {
                        if(err) return res.status(400).send({ message: "Somwthing went wrong while deleting chapter" });
                        
                        res.status(204).send(null);
                    });
                });
        });
};

/***** Defined local functions *****/
function addChapter(req, res, novel) {
    if(!(req.body && req.body.title && req.body.chapter))
        return res.status(400).send({ message: "Insufficient data: Title and/or content are missing" });
    
    Chapter.create({
        novel: novel._id,
        title: validateString(req.body.title),
        chapter: validateString(req.body.chapter)
    }, function(err, chapter) {
        if(err) {
            //console.log(err);
            return res.status(400).send({ message: "Inputted data is incorrect to create new chapter. Are all field filled in?" });
        }
        
        // Save index to novel
        novel.chapters.push(chapter._id);
        novel.save(function(err, novel) {
            if(err) {
                //console.log(err);
                return res.status(400).send({ message: "Something went wrong while saving novel with new chapter" });
            }
            
            res.status(201).send(chapter);
        });
    });
}
function validateString(rawString) {
    if(!rawString) return undefined;
    return rawString.replace(/<script/gmi, "&lt;script");
}