var mongoose = require('mongoose');
mongoose.Promise = Promise;
var utils = require("./_include/utils");
var Tags = require("./tags.controllers");
var User = mongoose.model('User');
var Novel = mongoose.model('Novel');

var request = require('request');


module.exports.login = function(req, res) {
    if(!(req.body && req.body.email && req.body.password))
        return res.status(400).send("Login data is missing");
    
    User.findOne(
        { email: req.body.email },
        function(err, user) {
            if(err) {
                //console.log(err);
                return res.status(404).send({ message: "Error retrieving user from DB" });
            }
            if(!user) return res.status(404).send("Requested email isn't in our database");
            
            // Check if password is valid
            user.validatePassword(req.body.password, function(err, valid) {
                if(err) {
                    //console.log(err);
                    return res.status(401).send({ auth: false, token: null });
                }
                if(!valid) return res.status(401).send({ auth: false, token: null });
                
                // If user is found and password is valid, create a token
                var remember_me = false;
                if(req.body.remember) remember_me = true;
                var token = user.genJwt(remember_me);
                
                res.status(200).send({ auth: true, token: token });
            }
        );
    });
};
module.exports.logout = function(req, res) {
    res.status(200).send({ auth: false, token: null });
};

function recaptchaValidate(req, res, next) {
    if(req.body && req.body.captcha) {
        var requestParameters = {
            url: "https://www.google.com/recaptcha/api/siteverify",
            method: "POST",
            form: {
                secret: process.env.CAPTCHA_SECRET,
                response: req.body.captcha
            }
        };
        request(requestParameters, function(err, response, captcha) {
            if(err != null) {
                //console.log(err)
                return res.status(403).send({ message: "Error while validating ReCaptcha" });
            }
            
            captcha = JSON.parse(captcha);
            if(captcha.success != true)
                return res.status(400).send({ message: "You have provided an invalid captcha" });
            
            next(req, res);
        });
    } else {
        return res.status(400).send({ message: "You haven't been validated using google's recaptcha" });
    }
}
function createUser(req, res) {
    User.create(
        {
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        }, function(err, user) {
            if(err) {
                //console.log(err);
                return res.status(403).send({ message: "Encountered error while creating a new user" });
            }
            
            var token = user.genJwt();
            res.status(201).send({ auth: true, token: token });
        }
    );
}

module.exports.addUser = function(req, res) {
    
    if(!(req.body && req.body.name && req.body.email && req.body.password))
        return res.status(400).send({ message: "Data provided for registering is incomplete" });
    
    if(!utils.isEmail(req.body.email))
        return res.status(400).send({ message: "Provided email is invalid" });
    
    // Check if user with this email already exists
    User.findOne({email: req.body.email}, function(err, user) {
        if(err) {
            //console.log(err);
            return res.status(404).send({ message: "Encountered error while looking for duplicate email" });
        }
        if(user) return res.status(409).send({ message: "This email already exists in our database" });
        
        recaptchaValidate(req, res, createUser);
    });
};
module.exports.getUsers = function(req, res) {
    User
        .find()
        .exec(function(err, users) {
            if(err) {
                //console.log(err)
                return res.status(400).send({ message: "Encountered error while retrieving users" });
            }
            
            res.status(200).send(users);
        });
};
module.exports.getUserMe = function(req, res) {
    req.params.user_id = req.user._id;
    module.exports.getUser(req, res);
};
module.exports.getUser = function(req, res) {
    User
        .findById(req.params.user_id)
        .select("-password -__v")
        .exec(function(err, user) {
            if(err) {
                // console.log(err);
                return res.status(404).send({ message: "Requested user cannot be found" });
            }
            if(!user) return res.status(404).send("Can't find requested user in DB");
            
            res.status(200).send(user);
        });
};
module.exports.updateUser = function(req, res) {
    User
        //.findById(req.user.id)
        .findById(req.user._id)
        .exec(function(err, user) {
            if(err) {
                // console.log(err);
                return res.status(404).send("Requested user cannot be found");
            }
            if(!user) return res.status(404).send("Can't find current user in DB");
            
            // Now update user
            if(req.body.name)
                user.name = req.body.name;
            if(req.body.password)
                user.password = req.body.password;
            
            if(req.body.email && req.body.email != user.email) {
                // Look for duplicate !!
                User.findOne({ email: req.body.email })
                    .exec(function(err, exist) {
                        if(err) {
                            //console.log(err);
                            return res.status(400).send({ message: "Encountered error while checking for user duplicate" });
                        }
                        if(exist) return res.status(409).send("User with this email already exists");
                        
                        user.email = req.body.email;
                        
                        user.save(function(err, user) {
                            if(err) {
                                //console.log(err);
                                return res.status(400).send({ message: "Encountered error while saving user" });
                            }
                            
                            res.status(200).send(user);
                        });
                    });
            } else {
                user.save(function(err, user) {
                    if(err) return res.status(500, err);
                    
                    res.status(200).send(user);
                });
            }
        });
};
module.exports.deleteUser = function(req, res) {
    User
        .findByIdAndRemove(req.user._id)
        .exec(function(err, user) {
            if(err) {
                //console.log(err);
                return res.status(404).send({ message: "Requested user cannot be found" });
            }
            
            res.status(204).send(null);
        });
};

module.exports.addToLibrary = function(req, res) {
    User
        .findById(req.user._id)
        .exec(function(err, user) {
            if(err) {
                //console.log(err);
                return res.status(403).send({ message: "Encountered error while retrieving user" });
            }
            if(!user) return res.status(404).send("Are you trying to cheat the system? You aren't supposed to exist!");
            
            for(var i = 0; i < user.library.length; i++) {
                if(user.library[i] == req.body.novel_id)
                    return res.status(409).send({ message: "Already exists" });
            }
            
            Novel
                .findById(req.body.novel_id)
                .select("_id title")
                .exec(function(err, novel) {
                    if(err) {
                        //console.log(err);
                        return res.status(403).send({ message: "Encountered error while retrieving novel" });
                    }
                    if(!novel) return res.status(404).send("Requested novel couldn't be found");
                    
                    user.library.push(novel._id);
                    user.save(function(err, user) {
                        if(err) {
                            //console.log(err);
                            return res.status(403).send({ message: "Error while saving updated user data" });
                        }
                        res.status(200).send(user);
                    });
                });
        });
};
module.exports.getLibrary = function(req, res) {
    User
        .findById(req.user._id)
        .populate({
            path: "library",
            select: "-chapters -reviews",
            populate: {
                path: "author",
                select: "name"
            }
        })
        .select("_id name")
        .exec(function(err, user) {
            if(err) {
                console.log(err);
                return res.status(403).send({ message: "Encountered error while retrieving user's library" });
            }
            if(!user) {
                return res.status(403).send({ message: "User doesn't exist" });
            }
            // Now also submit all tags
            
            res.novels = user.library;
            Tags.getTags(req, res);
        });
};
module.exports.removeFromLibrary = function(req, res) {
    if(!req.body.novel_id) return res.status(400).send({ message: "Novel_id required to add novel to the library" });
    
    User
    .findById(req.user._id)
    .exec(function(err, user) {
        if(err) {
            //console.log(err);
            return res.status(403).send({ message: "Encountered error while retrieving user" });
        }
        if(!user) return res.status(404).send("Requested user doesn't exist");
        
        Novel
            .findById(req.body.novel_id)
            .select("_id title")
            .exec(function(err, novel) {
                if(err) {
                    //console.log(err);
                    return res.status(403).send({ message: "Encountered error while retrieving novel" });
                }
                if(!novel) return res.status(404).send("Couldn't find this novel");
                
                for(var i = 0; i < user.library.length; i++) {
                    if(user.library[i] == req.body.novel_id) {
                        user.library.remove(novel._id);
                    }
                }
                
                user.save(function(err, user) {
                    if(err) {
                        //console.log(err);
                        return res.status(403).send({ message: "Encountered error while saving library" });
                    }
                    
                    res.status(200).send(user);
                });
            });
    });
};
module.exports.removeFromLibrary2 = function(req, res) {
    User
    .findById(req.user._id)
    .exec(function(err, user) {
        if(err) {
            //console.log(err);
            return res.status(403).send({ message: "Encountered error while retrieving user" });
        }
        if(!user) return res.status(404).send("Requested user doesn't exist");
        
        Novel
            .findById(req.params.novel_id)
            .select("_id title")
            .exec(function(err, novel) {
                if(err) {
                    //console.log(err);
                    return res.status(403).send({ message: "Encountered error while retrieving novel" });
                }
                if(!novel) return res.status(404).send("Couldn't find this novel");
                
                for(var i = 0; i < user.library.length; i++) {
                    if(user.library[i] == req.params.novel_id) {
                        user.library.remove(novel._id);
                    }
                }
                
                user.save(function(err, user) {
                    if(err) {
                        //console.log(err);
                        return res.status(403).send({ message: "Encountered error while saving library" });
                    }
                    
                    res.status(200).send(user);
                });
            });
    });
};