
// Calculates and returns the average value of all objects 'object' with variable 'where_is_value' --> object[where_is_value]
module.exports.calcAvg = function(object, value_field) {
    var sum, i;
    if(!object) {
        console.log("Trying to calcAvg for invalid object");
        return 0;
    } else if(!Array.isArray(object)) {
        console.log("calcAvg: Specified object isn't an array!");
        return 0;
    } else {
        if(value_field == null | undefined) {
            sum = 0;
            for(i = 0; i < object.length; i++)
                sum += object[i];
        } else {
             if(!object[0][value_field]) {
                console.log("Specified value_field doesn't exist for this object - value_field: " + value_field);
                return 0;
             }
            sum = 0;
            for(i = 0; i < object.length; i++)
                sum += object[i][value_field];
        }
        return sum / object.length;
    }
};

// Check if provided string is formatted as a proper EMAIL
module.exports.isEmail = function(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
};
