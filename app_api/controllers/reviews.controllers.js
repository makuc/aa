var mongoose = require('mongoose');
mongoose.Promise = Promise;
var utils = require("./_include/utils");
var Novel = mongoose.model('Novel');

module.exports.addReview = function(req, res) {
    Novel
        .findById(req.params.novel_id)
        .select("reviews")
        .exec(function(err, novel) {
            if(err) {
                //console.log(err);
                return res.status(404).send({ message: "Requested novel cannot found" });
            }
            if(!novel) return res.status(404).send({ message: "Requested novel cannot be found" });
            
            addReview(req, res, novel);
        });
};
module.exports.getReview = function(req, res) {
    Novel
        .findById(req.params.novel_id)
        .select('title reviews')
        .exec(function(err, novel){
            if(err) {
                //console.log(err);
                return res.status(403).send({ message: "Encountered error while retrieving novel" });
            }
            if(!novel) return res.status(404).send({ message: "Requested novel cannot be found" });
            if(novel.reviews.length == 0) return res.status(404).send({ message: "Requested novel has no reviews, including requested one" });
            
            var result, review;
            
            // Find requested review, if reviews exist
            review = novel.reviews.id(req.params.review_id);
            if(!review) return res.status(404).send({ message : "Requested review doesn't exist" });
            
            result = {
                "novel" : { "title" : novel.title, "novel_id" : novel._id },
                "review" : review
            };
            res.status(200).send(result);
        });
};
module.exports.updateReview = function(req, res) {
    Novel
        .findById(req.params.novel_id)
        .select("reviews")
        .exec(function(err, novel) {
            if(err) {
                //console.log(err);
                return res.status(404).send({ message: "Requested novel cannot found" });
            }
            if(!novel) return res.status(404).send({ message: "Requested novel cannot be found" });
            
            var review;
            
            if(novel.reviews.length == 0) return res.status(404).send({ message: "Requested novel has no reviews, including the requested one" });
            
            review = novel.reviews.id(req.params.review_id);
            if(!review) res.status(404).send({ message: "Requested review cannot be found" });
            
            // Check if user has permission to update this review
            if(review.author != req.user._id) return res.status(403).send({ message: "You don't have permission to update this review" });
            
            if(req.body.rating)
                review.rating = validateRating(req.body.rating);
            if(typeof req.body.review === "string")
                review.review = req.body.review.replace(/<script/gmi, "&lt;script");
            
            review.date = Date.now();
            
            novel.save(function(err, novel) {
                if(err) {
                    //console.log(err);
                    return res.status(400).send({ message: "Review failed to be saved. You could try again later..." });
                }
                
                updateAverageRating(req.params.novel_id);
                res.status(200).send(review);
            });
        });
};
module.exports.deleteReview = function(req, res) {
    Novel
        .findById(req.params.novel_id)
        .select("reviews")
        .exec(function(err, novel) {
            if(err) {
                //console.log(err);
                return res.status(404).send({ message: "Requested novel cannot found" });
            }
            if(!novel) return res.status(404).send({ message: "Requested novel cannot be found" });
            if(novel.reviews.length == 0) return res.status(404).send({ message: "Requested novel has no reviews, including requested one" });
            
            var review = novel.reviews.id(req.params.review_id);
            
            if(!review) res.status(404).send({ message: "Requested review cannot be found" });
            
            // Check if user has permission to delete this review
            if(review.author != req.user._id) return res.status(403).send({ message: "You don't have permission to delete this review" });
            
            review.remove();
            novel.save(function(err, novel) {
                if(err) {
                    //console.log(err);
                    return res.status(400).send({ message: "Encountered error while deleting review" });
                }
                
                updateAverageRating(novel._id);
                res.status(204).send();
            });
        });
};

/***** Functions... Bean? *****/
var addReview = function(req, res, novel) {
    if(typeof req.body.review === "string") {
        var review = req.body.review.replace(/<script/gmi, "&lt;script");
    }
    novel.reviews.push({
        "author": req.user._id,
        "rating": validateRating(req.body.rating),
        "review": review
    });
    novel.save(function(err, novel) {
        if(err) {
            //console.log(err);
            return res.status(400).send({ message: "A valid rating is required" });
        }
        
        var review;
        updateAverageRating(novel._id);
        review = novel.reviews[novel.reviews.length - 1];
        res.status(201).send(review);
    });
};
var updateAverageRating = function(novel_id) {
    Novel
        .findById(novel_id)
        .select("rating reviews")
        .exec(function(err, novel) {
            if(err) {
                //console.log(err);
                console.log("Encountered an error while updating average rating: novel not found");
                return;
            }
            if(!novel) return console.log("Requested novel seems to have been deleted in the meantime");
            
            novel.rating = utils.calcAvg(novel.reviews, "rating");
            novel.save(function(err) {
                if(err) {
                    console.log("Cannot save updated average rating - error while saving novel:\n" + err);
                    return;
                }
                
                console.log("Average rating for novel_id: " + novel._id + " updated to: " + novel.rating.toFixed(3));
            });
        });
};
function validateRating(rating) {
    if(rating) {
        try {
            return parseInt(rating, 10);
        } catch(e) {
            
        }
    }
    return undefined;
}