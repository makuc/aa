var mongoose = require('mongoose');
mongoose.Promise = Promise;
var Novel = mongoose.model('Novel');

// Parses tags - splits them using ',' and ';', trims leading and following spaces and removes empty tags
module.exports.parseTags = function(tagsObject) {
    if(!tagsObject) return undefined;
    
    var tag, tags = [], i, j;
    
    if(Array.isArray(tagsObject)) {
        for(i = 0; i < tagsObject.length && Array.isArray(tagsObject); i++) {
            tagsObject[i] = module.exports.parseTags(tagsObject[i]);
            for(j = 0; j < tagsObject[i].length; j++)
                tags.push(tagsObject[i][j]);
        }
    } else if(typeof tagsObject == "string") {
        tag = tagsObject;
        tag = tag.split(/[,;]+/);
        
        for(i = 0; i < tag.length; i++) {
            tag[i] = tag[i].trim();
            tag[i] = tag[i].replace(/\s/g, '-');
            tag[i] = tag[i].replace(/[\[\]\{\}\(\)\*\+\?\.\\\^\$\|\#\]\<\>]/g, '');
            if(tag[i] !== "") {
                tag[i] = tag[i].trim();
                if(tags.indexOf(tag[i]) == -1)
                    tags.push(tag[i]);
            }
        }
    }
    return tags;
};
module.exports.getTags = function(req, res) {
    var result = {};
    if(res.novels)
        result.novels = res.novels;
    
    Novel
        .distinct("tags")
        .exec(function(err, tags) {
            if(err) {
                console.log(err);
                return res.status(404).send({ message: "Tags not found" });
            }
            
            result.tags = tags;
            
            res.status(200).send(result);
        });
};