var mongoose = require('mongoose');
mongoose.Promise = Promise;
var Tags = require("./tags.controllers");
var Novel = mongoose.model('Novel');
var User = mongoose.model('User');

// Get all novels from the database
module.exports.getMyNovels = function(req, res) {
    Novel
        .find({ author: req.user._id })
        .populate([
            {
                path: "author",
                select: "name -_id"
            }
        ])
        .select("-chapters -reviews -__v")
        .exec(function(err, novels) {
            if(err) {
                //console.log(err);
                return res.status(404).send({ message: "Novels not found" });
            }
            if(!novels) return res.status(404).send({ message: "No novels found" });
            
            res.novels = novels;
            Tags.getTags(req, res);
        });
};
module.exports.getNovels = function(req, res) {
    var validTags = Tags.parseTags(req.query.g || req.body.g);
    var validSearch = validateSearch(req.body.s || req.query.s);
    
    if(validTags && validSearch) {
        getNovelsSearchAndTags(req, res);
    } else if(validTags) {
        getNovelsTags(req, res);
    } else if(validSearch) {
        getNovelsSearch(req, res);
    } else {
        getNovels(req, res);
    }
};
module.exports.addNovel = function(req, res) {
    if(!(req.body && req.body.title))
        return res.status(400).data({ message: "Title for a new novel is required" });
    
    var tags = [];
    if(req.body.tags) {
        tags = Tags.parseTags(req.body.tags);
    }
    Novel.create({
        "title": validateString(req.body.title),
        "description": validateString(req.body.description),
        "tags": tags,
        "author": req.user._id
    }, function(err, novel) {
        if(err) {
            //console.log(err);
            return res.status(400).send({ message: "Entered data is incorrect" });
        }
        
        res.status(201).send(novel);
    });
};
module.exports.getNovel = function(req, res) {
    Novel
        .findById(req.params.novel_id)
        .populate([
            {
                path: "author",
                select: "name"
            },
            {
                path: "reviews.author",
                select: "name -_id"
            },
            {
                path: "chapters",
                select: "title _id"
            }
        ])
        .select("-__v")
        .exec(function(err, novel) {
            if(err) {
                //console.log(err);
                return res.status(404).send({ message: "Novel not found" });
            }
            if(!novel) return res.status(404).send({ message: "Can't find novel with this novel_id" });
            
            if(req.user) {
                User
                    .findOne({ _id: req.user._id })
                    .select("library")
                    .exec(function(err, user) {
                        if(err) {
                            //console.log(err);
                            return res.status(200).send(novel);
                        }
                        if(!user) return res.status(200).send(novel);
                        
                        for(var i = 0; i < user.library.length; i++) {
                            if(user.library[i] == req.params.novel_id) {
                                novel = novel.toObject();
                                novel.library = true;
                            }
                        }
                        res.status(200).send(novel);
                    });
            } else
                res.status(200).send(novel);
        });
};
module.exports.updateNovel = function(req, res) {
    Novel
        .findById(req.params.novel_id)
        .populate("author", "name -_id")
        .select('-rating -chapters -reviews -__v')
        .exec(function(err, novel) {
            if(err) {
                //console.log(err);
                return res.status(404).send({ message: "Requested novel cannot be found" });
            }
            if(!novel) return res.status(404).send({ message: "Can't find requested novel" });
            
            if(req.body.title)
                novel.title = validateString(req.body.title);
            if(req.body.description)
                novel.description = validateString(req.body.description);
            if(req.body.tags)
                novel.tags = Tags.parseTags(req.body.tags);
            
            if(req.user._id == novel.populated("author")) {
                novel.save(function(err, novel) {
                    if(err) {
                        //console.log(err);
                        return res.status(400).send({ message: "Something went wrong. Is your data correct?" });
                    }
                    
                    res.status(200).send(novel);
                });
            } else {
                res.status(403).send({ message: "You don't have persmission to update this novel" });
            }
        });
};
module.exports.deleteNovel = function(req, res) {
    Novel
        .findById(req.params.novel_id)
        .exec(function(err, novel) {
            if(err) {
                //console.log(err);
                return res.status(404).send({ message: "Requested novel cannot be found" });
            }
            if(!novel) return res.status(404).send({ message: "Requested novel cannot be found." });
            
            if(req.user._id == novel.author) {
                
                /*
                /
                / Add removing all chapters of this novel !!!
                /
                */
                
                novel.remove(function(err) {
                    if(err) {
                        //console.log(err);
                        return res.status(400).send({ message: "Something went wrong. Is your request correct?" });
                    }
                    
                    res.status(204).send(null);
                });
            } else {
                res.status(403).send({ message: "You don't have permission to delete this novel" });
            }
        });
};

function getNovels(req, res) {
    Novel
        .find()
        .populate([
            {
                path: "author",
                select: "name -_id"
            }
        ])
        .select("-chapters -reviews -__v")
        .exec(function(err, novels) {
            if(err) {
                //console.log(err);
                return res.status(404).send({ message: "Novels not found" });
            }
            if(!novels) return res.status(404).send({ message: "Novels not found" });
            
            res.novels = novels;
            Tags.getTags(req, res);
        });
}
function getNovelsTags(req, res) {
    var tags = req.query.g || req.body.g;
    Novel
        .find({
            tags: {$all: tags}
        })
        .populate([ // Can probably delete this, but I won't touch it for now !!!
            {
                path: "author",
                select: "name -_id"
            }
        ])
        .select("-chapters -reviews -__v")
        .exec(function(err, novels) {
            if(err) {
                //console.log(err);
                return res.status(403).send({ message: "Encountered error while retrieving novels" });
            }
            if(!novels) return res.status(404).send({ message: "Novel not found" });
            
            res.novels = novels;
            Tags.getTags(req, res);
        });
}
function getNovelsSearch(req, res) {
    var words = req.query.s || req.body.s;
    
    Novel
        .find({
            title: {
                $regex: words,
                $options: 'i'
            }
        })
        .populate([
            {
                path: "author",
                select: "name -_id"
            }
        ])
        .select("-chapters -reviews -__v")
        .exec(function(err, novels) {
            if(err) {
                //console.log(err);
                return res.status(403).send({ message: "Encountered error while retrieving/searching novels" });
            }
            if(!novels) return res.status(404).send({ message: "Novel not found" });
            
            res.novels = novels;
            Tags.getTags(req, res);
        });
}
function getNovelsSearchAndTags(req, res) {
    var tags = req.query.g || req.body.g;
    var words = req.query.s || req.body.s;
    
    Novel
        .find({
            tags: {$all: tags},
            title: {
                $regex: words,
                $options: 'i'
            }
        })
        .populate([
            {
                path: "author",
                select: "name -_id"
            }
        ])
        .select("-chapters -reviews -__v")
        .exec(function(err, novels) {
            if(err) {
                //console.log(err);
                return res.status(403).send({ message: "Encountered error while retrieving/searching novels"});
            }
            if(!novels) return res.status(404).send({ message: "Novel not found" });
            
            res.novels = novels;
            Tags.getTags(req, res);
        });
}

function validateSearch(rawSearch) {
    if(!rawSearch) return undefined;
    return rawSearch;
}
function validateString(rawString) {
    if(!rawString) return undefined;
    return rawString.replace(/<script/gmi, "&lt;script");
}