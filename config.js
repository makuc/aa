// Configuration file
module.exports = {
    // Development environment parameters
    "devServer": "http://localhost:",
    "devDB": "mongodb://localhost/aa-novels",
    
    // Production environment parameters
    "prodServer": "https://aa-novels.herokuapp.com",
    "prodDB": ""
};
