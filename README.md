# ArdentAngel - Za amaterske pisatelje
## Za koga?
Spletna stran namenjena vsem tistim strastnim ljubiteljem knjig, ki so se odločili narediti tisti naslednji korak v svojem domišljijskem svetu in ga predstaviti širšemu svetu - od strastnih bralcev za strastne bralce.

Čeprav nihče od amaterjev ne pričakuje mojstrovine, to še ne pomeni, da se mojstrovina ne more roditi iz rok amaterja. Kdo ve? Mogoče boš pa prav ti naslednji Dan Brown ali Christopher Paolini!
## Kaj?
Preprost vmesnik omogoča prav vsakemu registriranemu uporabniku hitro stvaritev svoje spletne zgodbe, ki je nemudoma objavljena, prav tako kot preprosto brskanje po še objavljenih zgodbah ali iskanje po željenih žanrih.

## Poročilo ##
### Logiranje uporabnikov ###
Na heroku aplikacijo sem namestil razširitev "logDNA", ki naj bi skrbela za hranjenje logev iz Heroku, hkrati pa mi omogoča izboljšan pregled nad njimi, saj ima že vgrajeno filtriranje in iskanje, s čimer hitreje najdem iskane informacije.

### ZAP Poročilo ###
ZAP Poročilo se nahaja na sledečem naslovu: [ZAP Poročilo](/docs/reports/zap-report.html)

### JMeter Rezultati ###
#### Aggregate Graph ####
![Aggregate Graph](docs/jmeter/aggregate-graph.png)

#### Graph Results ####
![Graph Results](docs/jmeter/graph-results.png)

#### Response Time Graph ####
![Response Time Graph](docs/jmeter/response-time-graph.png)

### Selenium (razširitev Katalon) ####
[Katalon program](/docs/reports/katalon.html)

### Čas nalaganj posameznih delov strani ###
V Chrome in Firefox brskalniku sem testiral nalaganje spletne strani in prišel do ugotovitve, da se strani za prikaz podrobnosti zgodbe nalagajo največ časa, zaradi kompleksnosti HTML dokumenta (veliksot noscript tag-a), in v kolikor je dolžina posameznega poglavja daljša, potem je to tudi stran za prikaz poglavij.
Zgornje velja v kolikor je stran statična (torej ko se stran nalaga prvič). Ko se število zgodb poveča, potem se poveča nalaganje strani Browse in lahko dokaj hitro postane večja od prejšnjih dveh.

V SPA aplikaciji pa se največ časa nalaga stran z zgodbami (torej Browse), ker trenutna implementacija nima vgrajene omejitve zadetkov in zatorej prenese vse zgodbe it strežnika. Jasno, če je zgodb malo, to ni tako opazno.

Podatki niso nujno točni, saj sem meritve izvajal doma, kjer nimamo optike (internet po telefonski liniji).

Povprečno nalaganje strani na heroku strežniku (javascript on, brisan cache):
  1. Browse: 6s (prikaz z 2 zgodbama)
  2. Search: 5.8s (prikaz z eno zgodbo)
  3. Novel's details: 5s
  4. Novel's chapter: 5.5s
  5. About: 4.7s
  6. DB Mng: 4.43

Povprečno nalaganje strani na heroku strežniku (javascript on, SPA menjanje strani):
  1. Browse: 6s (prikaz z 2 zgodbama)
  2. Search: 5.8s (prikaz z eno zgodbo)
  3. Novel's details: 5s
  4. Novel's chapter: 5.5s
  5. About: 4.7s
  6. DB Mng: 4.43

Povprečno nalaganje strani na heroku strežniku (javascript off, brisan cache):
  1. Browse: 2.7s (prikaz z 2 zgodbama)
  2. Search: 2.6s (prikaz z eno zgodbo)
  3. Novel's details: 2.7s
  4. Novel's chapter: 2.6s
  5. About: 1.32s
  6. DB Mng: 2.10s

### Namestitev/Posodobitev najnovejše različice MongoDB podatkovne baze ###
  1. Poženi ukaz: sudo apt-get remove mongodb-org mongodb-org-server
  2. Poženi ukaz: sudo apt-get autoremove
  3. Poženi ukat: sudo rm -rf /usr/bin/mongo*
  4. Poženi ukaz: echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
  5. Poženi ukaz: sudo apt-get update
  6. Poženi ukaz: sudo apt-get install mongodb-org mongodb-org-server
  7. Poženi ukaz: sudo touch /etc/init.d/mongod
  8. Poženi ukaz: sudo apt-get install mongodb-org-server
  
  9. Po želji se pomakni v korenski imenik, če želiš tam ustvariti mapo za podatkovno bazo: cd ~/workspace
  10. Kreiraj mapo za lokacijo podatkovne baze: mkdir mongodb
  11. Poženi ukaz: cd mongodb
  12. Poženi ukaz: mkdir data
  13. Poženi ukaz: echo 'mongod --bind_ip=$IP --dbpath=data --nojournal --rest "$@"' > mongod
  14. Poženi ukaz: chmod a+x mongod
  15. Poženi ukaz: ./mongod

### Kako namestiti Node.js strežnik in ga pognati ###
  1. Kloniraj git repozitorij v lokalno okolje z ukazom "git clone https://makuc@bitbucket.org/makuc/sp-v1.git".
  2. Navigiraj terminal v ustrezno podmapo: "cd sp-v1"
  3. Poženi "npm install" ukaz za namestitev node.js in prenos ustreznih paketov "packages".
  4. V datoteki "config.js" v korenski mapi repozitorija posodobi spremenljivke: "devDB" in "devServer", v kolikor je to potrebno. (če bo strežnik pognan lokalno, potem jih ni treba spreminjati)
  5. Poskrbi, da je "mongodb" podatkovna baza pognana.
  6. Poženi Node.js strežnik z ukazom "npm start"
  7. Obišči naslov predogleda aplikacije (naslov v c9.io lahko ugotoviš s: 'Preview' --> 'Preview running applications')
  8. Obišči podnasloc: "/db" na korenski URL naslov, pridobljen v podoknu "Preview running applications"
  9. Klikni na: "Drop database"
  10. Klikni na: "Back to DB management"
  11. Klikni na: "Populate database with dummy data"
  12. Obišči domačo stran s klikon na ime strani ali katerokoli drugo stran v glavnem meniju!
  13. Uživaj v delujoči spletni splikaciji!

### Razlike v pogledih brskalnikov ###
Testirav sem v brskalnikih Microsoft Edge, Mozilla Firefox 57 in Chrome brskalniku. Opazil nisem nobenih opaznejših razlik med prikazom, kar predpisujem bootstrap razširitvi, ki je prikaz poenotila.
Namreč iz izkušenj vem, da ima vsak brskalnik neke svoje privzete nastavitve za sloge dokumentov HTML katere je ročno popraviti prava muka.

### Privzeta glavna (index) stran ###
Ker sem template strani izdelav v JADE template obliki, se kot prva stran izriše template: novels-browse.jade (podmapa: "app_server/views/").
Vse ostale podstrani so dosegljive iz le-te.

Namestitev na heroku strežniku: https://aa-novels.herokuapp.com/


# Zaslonske maske #

Prvotna zasnova - fokus na podatkih, ki so potrebni na posamezni zaslonski maski.

## Uporabniki
### Registracija uporabnikov
Kot večina spletnih strani za delo z raznovrstnimi uporabniki, tudi tale potrebuje registracijo za vse morebitne bodoče avtorje. Za branje samo sicer le-to ni potrebno, vendar pa imajo registrirani uporabniki kljub temu dodatne funkcionalnosti, ki izboljšajo uporabniško izkušnjo - knjižnjica za hitrejšo najdbo branih zgodb, da lahko lažje preberejo vsa nova poglavja, ki se bodo objavila naknadno, možnst komentiranja poglavij (mogoča nadgradnja) ter pisanje ocen (review-jev) za zgodbe same.
Prav tako se direktno kvalificirajo kot morebitni pisatelji za v prihodnost!

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![Registracija uporabnika - Desktop](docs/AA-Desktop-Sign-up.png) | ![Registracija uporabnika - Mobile](docs/AA-Mobile-Sign-up.png)

Za registracijo uporabnika potrebni samo "Full name" ali pisateljski psevdonim (uporablja pri komentiranju poglavij, pisanju review-jih ali pisanju zgodbe), e-pošta in geslo.

Za oveljavitev uporabnika se uporablja nevidna captcha.

### Prijava obstoječih uporabnikov

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![Prijava uporabnika - Desktop](docs/AA-Desktop-Sign-in.png) | ![Prijava uporabnika - Mobile](docs/AA-Mobile-Sign-in.png)

Ti dve zaslonski maski prikazujeta formo za prijavo obstoječih uporabnikov na stran, ter jim s tem omogoča uporabo strani tudi iz večih naprav hkrati! Zaenkrat, žal, še ne nameravam omogočiti funkcije za obnovo gesla, je pa na seznamu morebitnih nadgradenj!
## Uporabniška nadzorna plošča

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![User panel - Desktop](docs/AA-Desktop-User-Panel.png) | ![User panel - Mobile](docs/AA-Mobile-User-Panel.png)

Ko prijavljeni uporabnik klikne na svojo sličico v glavnem meniju, se mu odpre podmeni z dodatnimi opcijami, ki uporabnikom omogočajo odjavo ("Sign out"), dostop do svoje knjižnjice ("Library") in v prihodnosti morda celo do ("Settings"), kot je razvidno na zgornjih maskah.

## Glavni meni
Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![](docs/AA-Desktop-Expanded-Search-Field.png) | ![](docs/AA-Mobile-Expanded-Header.png)
V glavnem meniju v načinu za Namizne računalnike, je tudi iskalnik zgodb, ki se ob kliku za vnos iskalnih besed razširi (aktivira) | V mobilnem načinu, pa je glavni meni skrit in se odpre ob kliku na gumb s tremi vodoravnimi črtami v krogu!

## Kje najti objavljene zgodbe
Ker je stran namenjena objavljanju in branju knjig v spletu, je bistvenega pomena preprosto iskanje objavljenih zgodb, da jih lahko tako registrirani uporabniki kot tudi gosti najdejo brez večjih naporov. V ta namen je na voljo več načinov iskanja zgodb.
### Brskanje po zgodbah

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![Brskanje po zgodbah - Desktop](docs/AA-Desktop-Browse.png) | ![Brskanje po zgodbah - Mobile](docs/AA-Mobile-Browse.png)

Kot najbolj preprost način iskanja zgodb na strani predstavlja povezava na vseh straneh v meniju, imenovana "Browse", ki preusmeri na zgoraj prikazano podstran. Tukaj uporabnik najde vse zgodbe, ki so bile kadarkoli naložene na spletno stran
### Filtriranje zgodb po žanrih
Vse strani namenjene iskanju objavljenih zgodb, tako Brskanje po zgodbah zgoraj, kot tudi nadaljnje spodaj, imajo možnost Filtriranja po žanrih ("Filter genre"), kar omogoča zožati izbor prikazanih knjig za branje! Ko uporabnik klikne na povezave, se mu namreč odpre stran, ki mu omogoča preprosto izbiro vseh želenih žanrov!

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![Filtriranje žanrov - Desktop](docs/AA-Desktop-Browse-Filter-Genre.png) | ![Filtriranje žanrov - Mobile](docs/AA-Mobile-Browse-Filter-Genre.png)

### Iskanje zgodb s ključnimi besedami
Podobno kot pri "Brskanje po zgodbah", tudi iskanje s ključnimi besedami uporabniki opravljajo iz glavnega menija in sicer tako, da svoje ključne besede vnesejo v iskalnik v glavnem meniju in pritisnejo gumb za iskanje (ikona z lupo). Le to jih nemudoma preusmeri na spodaj prikazano stran, kjer lahko najdejo zgodbe prikazane na podoben način, kot pri "Brskanje po zgodbah".

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![Iskanje zgodb - Desktop](docs/AA-Desktop-Search.png) | ![Iskanje zgodb - Mobile](docs/AA-Mobile-Search.png)

Tudi tu lahko s klikom na "Filter genre" še dodatno zožajo lasten izbor prikazanih zgodb.

### Iskanje po knjižnjici ("Library") zgodb
Vsak uporabnik ima svojo knjižnjico prebranih zgodb, do katere lahko dostopa iz svoje uporabniške plošče, kot je omenjeno zgoraj. Le-ta deluje podobno kot samo brskanje po zgodbah, le da tokrat uporabnik brska le po svojih prebranih zgodbah - iščoč morebitnih novih poglavij!

Kot bodoča nadgradnja je prikaz novih objavljenih poglavij odkar je določeno zgodbo uporabnik brav / dodal v knjižnjico, oziroma da si stran zapomne kje je uporabnik končal brati določeno knjigo, da mu olajša nadaljevanje branja od tam, kjer je končal, ko se odloči nadaljevati z branjem.

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![Moja knjižnjica - Desktop](docs/AA-Desktop-My-Novels.png) | ![Moja knjižnjica - Mobile](docs/AA-Mobile-My-Novels.png)

## Predogled zgodb
Ko uporabnik izbere določeno zgodbo in jo odpre, si lahko najprej ogleda povzetek zgodbe, žanre, ki jih zgodba zajema, ocene in review-je ostalih uporabnikov, ali celo napiše svojega (review).

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![Opis zgodbe - Desktop](docs/AA-Desktop-Novel-Description.png) | ![Opis zgodbe - Mobile](docs/AA-Mobile-Novel-Description.png)

Ko ima uporabnik odprt opis neke zgodbe, si lahko tudi ogleda poglavja, ki so objavljena v njej. To si lahko odpre tako, da klikne na ustrezno ikono v stranskem meniju in si odpre seznam s poglavji!

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![Opis zgodbe, poglavja - Desktop](docs/AA-Desktop-Novel-Description-Chapters.png) | ![Opis zgodbe, poglavja - Mobile](docs/AA-Mobile-Novel-Description-Chapters.png)

## Branje zgodbe
Ko se uporabnik odloči brati knjigo, s klikom na prvo poglavje, se mu odpre poglavje.

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![Branje poglavja - Desktop](docs/AA-Desktop-Chapter-Content.png) | ![Branje poglavja - Mobile](docs/AA-Mobile-Chapter-Content.png)

Bralec ima med branjem na voljo tudi stranski meni, ki si ga lahko odpre s klikom kjerkoli na debeli podolgovati navpični črti na desni strani (s puščico na vertikalni sredini). Ko si jo enkrat odpre, le-ta ostane odprta, dokler bralec ne nadaljuje z branjem.

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![Odprt stranski meni - Desktop](docs/AA-Desktop-Chapter-Content-Minimized.png) | ![Odprt stranski meni - Mobile](docs/AA-Mobile-Chapter-Content-Minimized.png)

Ta stranski meni mu ponuja več opcij. Najprej si lahko bralec ponovno ogleda poglavja, ki so že objavljena v trenutno brani zgodbi in skače med njimi.

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![Ogled poglavij - Desktop](docs/AA-Desktop-Chapter-Content-Chapters.png) | ![Ogled pogavij - Mobile](docs/AA-Mobile-Chapter-Content-Chapters.png)

Lahko pa tudi spremeni nastavitve prikaza poglavja (kot so pisava, velikost pisave, stil teme: svetla/temna) s klikom na *zobnik*, ob morebitni nadgradnji pa celo komentarje posameznega poglavja!

V kolikor je bralec poglavja tudi avtor zgodbe, ima na vrhu stranskega menija prikazano ikono "svinčnik" za urejanje poglavja. Ko ga klikne poteka urejanje poglavja enako kot pisanje poglavja spodaj!

## Pisanje poglavij
Za pisanje zgodbe je na voljo preprost vmesnik - avtor ima takojšen vpogled v to, kako bo njegovo poglavje zgledalo bralcem, saj piše kar znotraj poglavja samega, prav tako kot poteka tudi naknadno urejanje.

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![Pisanje poglavja - Desktop](docs/AA-Desktop-Write-Content.png) | ![Pisanje poglavja - Mobile](docs/AA-Mobile-Write-Content.png)

Poglavje lahko shrani z gumbi v stranskem meniju, kot lahko odpre tudi dodatne možnosti za pisave, torej **krepka pisava**, _poševna pisava_, vstavitev horizontalne mejne črte ali počiščenje vseh sprememb pisave.

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![Urejanje pisave - Desktop](docs/AA-Desktop-Write-Content-Font-Tools.png) | ![Urejanje pisave - Mobile](docs/AA-Mobile-Write-Content-Font-Tools.png)

Prav tako lahko tudi tukaj pisatelj skače med poglavji, če klikne na ikono za prikaz le-teh.

Namizni računalnik | Mobilni telefon
:-----------------:|:---------------:
![Prikaz poglavij - Desktop](docs/AA-Desktop-Write-Content-Chapters.png) | ![Prikaz poglavij - Mobile](docs/AA-Mobile-Write-Content-Chapters.png)

## Mogoče nadgradnje
Pod možne razširitve, v kolikor bo dovolj časa, spada možnost administratorskega/moderatorskega računa z obširno nadzorno ploščo, ki omogoča nadzor nad vsemi uporabniki, njihovimi objavami/komentarji, pregled/popravljanje objavljenih zgodb, in mogoča javna objava zgodb šele po odobritvi iz strani administratorja.