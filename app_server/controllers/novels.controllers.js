var request = require('request');
request = request.defaults({
  headers: {'static-page': 'true'}
});

var apiParameters = {
  server: "http://localhost:" + process.env.PORT
};
if(process.env.NODE_ENV === 'production') {
  apiParameters.server = "https://aa-novels.herokuapp.com";
}

/* Vrni začetno stran s seznamom lokacij */
module.exports.browse = function(req, res) {
  getNovelsList(req, res, function(req, res, novel) {
    displayNovelsList(req, res, novel);
  });
};
module.exports.search = function(req, res) {
    req.params.search = true;
    getNovelsList(req, res, function(req, res, novel) {
      displayNovelsList(req, res, novel);
    });
};
module.exports.library = function(req, res) {
  if(req.session.token) {
    req.library = true;
    getUserLibrary(req, res, function(req, res, novel) {
      displayNovelsList(req, res, novel);
    });
  } else {
    res.redirect("/login");
  }
};
module.exports.mynovels = function(req, res) {
  if(req.session.token) {
    req.myNovels = true;
    getNovelsList(req, res, function(req, res, novel) {
      displayNovelsList(req, res, novel);
    });
  } else {
    res.redirect("/login");
  }
};

/* Managing novels */
module.exports.details = function(req, res) {
  getNovelDetails(req, res, function(req, res, novel) {
    displayNovelsDetails(req, res, novel);
  });
};
module.exports.editDetails = function(req, res) {
  //if(req.session != undefined && req.session.token != undefined) res.redirect("/novels/" + req.params.novel_id);
  req.edit = true;
  getNovelDetails(req, res, function(req, res, novel) {
    displayNovelsDetails(req, res, novel);
  });
};
module.exports.addNovel = function(req, res) {
  displayNovelsDetailsAdd(req, res);
};
module.exports.addNovelSave = function(req, res) {
  var requestParameters, path, forwardedData;
  path = '/api/v1/novels';
  
  console.log(req.session.token);
  
  forwardedData = {
      title: req.body.title,
      description: req.body.description,
      tags: req.body.tags,
      token: req.session.token
  };
  requestParameters = {
      url: apiParameters.server + path,
      method: 'POST',
      json: forwardedData
  };
  request(
      requestParameters,
      function(error, answer, novel) {
          if(answer.statusCode == 201) {
              res.redirect('/novel/' + novel._id);
          } else {
              showError(req, res, answer.statusCode);
          }
      }
  );
};
module.exports.updateNovel = function(req, res) {
  var requestParameters, path, forwardedData;
  path = '/api/v1/novels/' + req.params.novel_id;
  
  forwardedData = {
      title: req.body.title,
      description: req.body.description,
      tags: req.body.tags,
      token: req.session.token
  };
  requestParameters = {
      url: apiParameters.server + path,
      method: 'PUT',
      json: forwardedData
  };
  request(
      requestParameters,
      function(error, answer, review) {
          if(answer.statusCode == 200) {
              res.redirect('/novel/' + req.params.novel_id);
          } else {
              showError(req, res, answer.statusCode);
          }
      }
  );
};
module.exports.deleteNovel = function(req, res) {
  var requestParameters, path;
  path = '/api/v1/novels/' + req.params.novel_id;
  
  requestParameters = {
    url: apiParameters.server + path,
    method: 'DELETE',
    json: {
      author: req.body.author
    }
  };
  request(
    requestParameters,
    function(error, answer, review) {
      console.log(answer.statusCode);
      if(answer.statusCode == 204) {
        res.redirect('/mynovels');
      } else {
        showError(req, res, answer.statusCode);
      }
    }
  );
};

module.exports.addToLibrary = function(req, res) {
  var requestParameters, path;
  path = '/api/v1/users/library';
  
  requestParameters = {
    url: apiParameters.server + path,
    method: 'POST',
    json: {
      novel_id: req.params.novel_id,
      token: req.session.token
    }
  };
  request(
    requestParameters,
    function(error, answer, review) {
      if(answer.statusCode == 200 || answer.statusCode == 409) {
        res.redirect('/novel/' + req.params.novel_id);
      } else {
        showError(req, res, answer.statusCode);
      }
    }
  );
};
module.exports.removeFromLibrary = function(req, res) {
  var requestParameters, path;
  path = '/api/v1/users/library';
  
  requestParameters = {
    url: apiParameters.server + path,
    method: 'DELETE',
    json: {
      novel_id: req.params.novel_id,
      token: req.session.token
    }
  };
  request(
    requestParameters,
    function(error, answer, review) {
      if(answer.statusCode == 200 || answer.statusCode == 409) {
        res.redirect('/novel/' + req.params.novel_id);
      } else {
        showError(req, res, answer.statusCode);
      }
    }
  );
};


/* Chapters */
module.exports.chapter = function(req, res) {
  getChapter(req, res, function(req, res, chapter) {
    displayChapter(req, res, chapter);
  });
};
module.exports.addChapter = function(req, res) {
  req.add = true;
  getNovelDetails(req, res, function(req, res, novel) {
    displayChapterAdd(req, res, novel);
  });
};
module.exports.addChapterSave = function(req, res) {
  var requestParameters, path, novel_id, forwardedData;
  novel_id = req.params.novel_id;
  path = '/api/v1/novels/' + novel_id;
  
  forwardedData = {
    title: req.body.title,
    chapter: req.body.chapter,
    token: req.session.token
  };
  requestParameters = {
    url: apiParameters.server + path,
    method: 'POST',
    json: forwardedData
  };
  request(
    requestParameters,
    function(error, answer, content) {
      console.log(content);
      if(answer.statusCode == 201) {
        res.redirect('/novel/' + novel_id + "/c/" + content._id);
      } else {
        showError(req, res, answer.statusCode);
      }
    }
  );
};
module.exports.updateChapter = function(req, res) {
  req.edit = true;
  getChapter(req, res, function(req, res, chapter) {
    displayChapter(req, res, chapter);
  });
};
module.exports.updateChapterSave = function(req, res) {
  var requestParameters, path, forwardedData;
  path = '/api/v1/novels/' + req.params.novel_id + "/" + req.params.chapter_id;
  
  forwardedData = {
    title: req.body.title,
    chapter: req.body.chapter,
    token: req.session.token
  };
  requestParameters = {
    url: apiParameters.server + path,
    method: 'PUT',
    json: forwardedData
  };
  request(
    requestParameters,
    function(error, answer, review) {
      if(answer.statusCode == 200) {
        res.redirect('/novel/' + req.params.novel_id + "/c/" + req.params.chapter_id);
      } else {
        showError(req, res, answer.statusCode);
      }
    }
  );
};
module.exports.deleteChapter = function(req, res) {
  var requestParameters, path;
  path = '/api/v1/novels/' + req.params.novel_id + "/" + req.params.chapter_id;
  requestParameters = {
    url: apiParameters.server + path,
    method: 'DELETE',
    json: {
      author: req.body.author
    }
  };
  request(
    requestParameters,
    function(error, answer, review) {
      if(answer.statusCode == 204) {
        res.redirect('/novel/' + req.params.novel_id);
      } else {
        showError(req, res, answer.statusCode);
      }
    }
  );
};

/***** Bean? *****/
function displayNovelsList(req, res, result) {
  var message, novels;
  if(result) {
    if(!(result.novels instanceof Array)) {
      message = "API error in retrieving list of novels";
      novels = [];
    } else {
      novels = result.novels;
    }
    if(novels.length == 0) {
      message = "No novels found!";
    }
  }
  
  var title, subtitle, tpl;
  tpl = 'novels-browse';
  if(result.my) {
    title = "My novels";
    subtitle = "Works I've written";
    tpl = 'novels-browse-my';
  } else if(result.library) {
    title = "Library";
    subtitle = "Stories I'm following";
  } else if(result.search) {
    title = "Search";
    subtitle = "A fine taste indeed!";
  } else {
    title = "Browse";
    subtitle = "Find that novel you're dying to read";
  }
  
  var search = "";
  if(req.query && req.query.s != undefined)
    search = req.query.s;
  
  res.render(tpl, {
    title: 'AA - ' + title,
    pageHeader: {
      title: title,
      subtitle: subtitle
    },
    genres: result.tags,
    pageFooter: {
      left: [
        {
          title: "Previous page",
          href: "#"
        },
        {
          title: "Next page",
          href: ""
        }
      ],
      right: "Page 1 / 1"
    },
    novels: novels,
    message: message,
    search: search
  });
}
function displayNovelsDetails(req, res, novel) {
  var editable = false;
  
  if(!req.edit && req.session.user !== undefined && req.session.user._id == novel.author._id)
    editable = true;
  
  res.render('novels-details', {
    title: "AA - " + novel.title,
    id: novel._id,
    pageHeader: {
      title: novel.title,
      author: novel.author.name
    },
    path: "/novel/" + req.params.novel_id,
    pageFooter: {
      left: [
        {
          title: "Browse",
          href: "/"
        },
        {
          title: novel.title,
          href: "/novel/" + req.params.novel_id
        }
      ],
      right: ""
    },
    details: {
      title: novel.title,
      description: novel.description,
      reviews: novel.reviews,
      tags: novel.tags,
      chapters: novel.chapters
    },
    editable: editable,
    edit: req.edit,
    library: novel.library
  });
}
function displayNovelsDetailsAdd(req, res) {
  res.render('novels-details', {
    title: "Create new novel",
    id: "add",
    pageHeader: {
      title: "Novel title...",
      author: "Your name will be here"
    },
    path: "/novel/add",
    pageFooter: {
      left: [
        {
          title: "Browse",
          href: "/"
        },
        {
          title: "Create new novel",
          href: "/novel/add"
        }
      ],
      right: ""
    },
    details: {
      description: "Write your description here",
      tags: [],
      chapters: []
    },
    edit: true
  });
}
function displayChapterAdd(req, res, novel) {
  res.render('novels-chapter', {
    title: "Add chapter - " + novel.title,
    novel: novel.title,
    author: novel.author,
    id: novel._id,
    pageFooter: {
      left: [
        {
          title: "Browse",
          href: "/"
        },
        {
          title: novel.title,
          href: "/novel/" + novel._id
        },
        {
          title: "Add new chapter",
          href: "/novel/" + novel._id + "/c"
        }
      ],
      right: ""
    },
    details: {
      title: "New title...",
      chapter: "Content...",
      date: Date.now(),
      chapters: novel.chapters
    },
    edit: true
  });
}
function displayChapter(req, res, chapter) {
  var editable = false;
  if(!req.edit && req.session.user !== undefined && req.session.user._id == chapter.novel.author._id)
    editable = true;
  
  res.render('novels-chapter', {
    title: "AA - " + chapter.title,
    novel: chapter.novel.title,
    author: chapter.novel.author,
    id: chapter.novel._id,
    pageFooter: {
      left: [
        {
          title: "Browse",
          href: "/"
        },
        {
          title: chapter.novel.title,
          href: "/novel/" + chapter.novel._id
        },
        {
          title: chapter.title,
          href: "/novel/" + chapter.novel._id + "/c/" + chapter._id
        }
      ],
      right: ""
    },
    details: {
      id: chapter._id,
      title: chapter.title,
      chapter: chapter.chapter,
      date: chapter.date,
      chapters: chapter.novel.chapters
    },
    edit: req.edit,
    editable: editable
  });
}
function getUserLibrary(req, res, callback) {
  var requestParameters, path;
  path = "/api/v1/library";
  requestParameters = {
    url: apiParameters.server + path,
    method: "GET",
    json: {
      token: req.session.token
    }
  };
  request(requestParameters, function(error, answer, result) {
    if(answer.statusCode == 200) {
      result.library = true;
      callback(req, res, result);
    } else {
      showError(req, res, answer.statusCode);
    }
  });
}
function getNovelsList(req, res, callback) {
  var requestParameters, path;
  path = "/api/v1/novels";
  if(req.myNovels)
    path = "/api/v1/my";
  
  requestParameters = {
    url: apiParameters.server + path,
    method: "GET",
    json: {
      token: req.session.token
    }
  };
  if(req.query.g)
    requestParameters.json.g = req.query.g;
  if(req.query.s)
    requestParameters.json.s = req.query.s;
  request(requestParameters, function(error, answer, result) {
    if(answer.statusCode == 200) {
      result.my = req.myNovels;
      result.search = req.params.search;
      callback(req, res, result);
    } else {
      showError(req, res, answer.statusCode);
    }
  });
}
function getNovelDetails(req, res, callback) {
  var requestParameters, path;
  path = "/api/v1/novels/" + req.params.novel_id;
  
  requestParameters = {
    url: apiParameters.server + path,
    method: 'GET',
    json: {
      token: req.session.token
    }
  };
  request(requestParameters, function(error, answer, novel) {
    if(answer.statusCode == 200) {
      callback(req, res, novel);
    } else {
      showError(req, res, answer.statusCode);
    }
  });
}
function getChapter(req, res, callback) {
  var requestParameters, path, editable;
  path = "/api/v1/novels/" + req.params.novel_id + "/" + req.params.chapter_id;
  editable = req.edit !== undefined;
  requestParameters = {
    url: apiParameters.server + path,
    method: 'GET',
    json: {}
  };
  request(requestParameters, function(error, answer, novel) {
    if(answer.statusCode == 200) {
      novel.edit = editable;
      callback(req, res, novel);
    } else {
      showError(req, res, answer.statusCode);
    }
  });
}

function showError(req, res, statusCode) {
    var title, content, subtitle;
    if(statusCode == 404) {
        title = "404";
        subtitle = "Page cannot be found";
        content = "Well, this is emberrasing";
    } else {
        title = statusCode + ", something went wrong";
        subtitle = "Something went wrong";
        content = "Apparently something somewhere doesn't work properly... Go figure...";
    }
    
    res.render('genericno-besedilo', {
        title: title,
        subtitle: subtitle,
        content: content
    });
}