// Check if provided string is formatted as a proper EMAIL
module.exports.isEmail = function(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
};

module.exports.showError = function(req, res, statusCode) {
    var title, content;
    if(statusCode == 404) {
        title = "404, This page cannot be found";
        content = "<p>Hmm... How is this possible? Did something go wrong? Because this page appears to be missing...</p>";
    } else if(statusCode == 401) {
        title = statusCode + ", you need to login to do that";
        content = "<p>Action you were trying to perform is only available to registered users!</p><p>Sorry for the inconvenience</p>";
        content += "<p><a href='/login'>You can click here to login</a></p";
    } else {
        title = statusCode + ", something went wrong";
        content = "<p>Apparently something somewhere doesn't work properly...</p>";
    }
    
    res.render('genericno-besedilo', {
        title: title,
        content: content
    });
};
