var request = require('request');
request = request.defaults({
  headers: {'static-page': 'true'}
});

var config = require('../../config');
var apiParameters = {
  server: config.devServer + process.env.PORT
};
if(process.env.NODE_ENV === 'production') {
  apiParameters.server = config.prodServer;
}

// Web Pages
module.exports.informacije = function(req, res) {
  var content = "";
  content += "<p>Website meant for ardent readers and even more passionate writers. It doesn't matter which you group you belong to, it's free to use for you all.</p>";
  content += "<p>You want to read novels? Go ahead, not even registering is necessary. Though by registering you do get a few more awesome features, like the ability to write reviews for all the novels you love, give them your rating, whose average is going to be shown on novel's description/details page. Or a Library, you get this feature, too. For free! It helps you keep track of all the novels you love, keeps them all grouped in a single place and makes them easier to find when you want them.</p>";
  content += "<p>As for all authors wanting to enrich the community, after a simple register, simply by clicking on User Panel and visiting \"Author\" page from there, a single click on a button \"Create a new novel\" is all that separates you from beginning your creative journey! Add a novel, or multiple novels if you so desire, and begin writing! A simple interface, no distractions! Just you, a page, and your imaginary world waiting to be written!</p>";
  content += "<p>Join us on our journey of enriching community of readers and writers!</p>";
  
  res.render('genericno-besedilo', {
      title: 'ArdentAngel',
      subtitle: "Stories, that's all that matters",
      content: content
  });
};

module.exports.db = function(req, res) {
  res.render('db', {
    title: "DB Management",
    subtitle: "Functions for setting up DB"
  });
};
module.exports.dbDrop = function(req, res) {
  req.session.destroy();
  var requestParameters, path;
  path = '/api/v1/db';
  
  requestParameters = {
    url: apiParameters.server + path,
    method: 'DELETE',
    json: {}
  };
  request(
    requestParameters,
    function(error, answer, review) {
      if(answer.statusCode == 204) {
        res.render('genericno-besedilo', {
          title: "Dropping database",
          subtitle: "Ain't I obvious enough?",
          content: "<p>Database successfully dropped...</p><p>Otherwise an error would have been shown!<p><a href='/db'>Back to DB management</a></p></p>"
        });
      } else {
        showError(req, res, answer.statusCode, error);
      }
    }
  );
};
module.exports.dbPopulate = function(req, res) {
  var requestParameters, path;
  path = '/api/v1/db';
  
  requestParameters = {
    url: apiParameters.server + path,
    method: 'POST',
    json: {}
  };
  request(
    requestParameters,
    function(error, answer, review) {
      console.log(answer.statusCode);
      if(answer.statusCode == 201) {
        res.render('genericno-besedilo', {
          title: "Populating database",
          subtitle: "Ain't I obvious enough?",
          content: "<p>Database successfully filled with previously prepared dummy data...</p><p>Otherwise an error would have been shown!</p><p><a href='/db'>Back to DB management</a></p>"
        });
      } else {
        showError(req, res, answer.statusCode, error);
      }
    }
  );
};

function showError(req, res, statusCode, error) {
    var title, content, subtitle;
    if(statusCode == 404) {
        title = "404";
        subtitle = "Page cannot be found";
        content = "Well, this is emberrasing";
    } else {
        title = statusCode + ", something went wrong";
        subtitle = "Something went wrong";
        content = "<p>Something could alwasy go wrong... And it just so happens that it did.</p><p>You could try dropping the database first.</p>";
    }
    
    res.render('genericno-besedilo', {
        title: title,
        subtitle: subtitle,
        content: content
    });
}
