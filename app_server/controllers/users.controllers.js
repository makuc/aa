var request = require('request');
request = request.defaults({
  headers: {'static-page': 'true'}
});

var apiParameters = {
  server: "http://localhost:" + process.env.PORT
};
if(process.env.NODE_ENV === 'production') {
  apiParameters.server = "https://aa-novels.herokuapp.com";
}

var utils = require("./_include/utils");

module.exports.loginForm = function(req, res) {
    if(req.session.token) {
        var redir = "/library";
        if(req.session.prevPage) {
            redir = req.session.prevPage;
            req.session.prevPage = undefined;
        }
        return res.redirect(redir);
    }
    
    var error = false;
    if(req.query.error !== undefined) {
        console.log("Error is set!");
        error = true;
    }
    res.render('user-login', {
        title: 'AA - Login',
        pageHeader: {
            title: 'Login',
            subtitle: "For best experience possible"
        },
        pageFooter: {
            left: [
                {
                    title: "Login",
                    href: "/login"
                }
            ],
            right: ""
        },
        error: error
    });
};
module.exports.login = function(req, res) {
    if(req.body.email && req.body.password) {
        var requestParameters, path;
        path = "/api/v1/login";
        
        requestParameters = {
            url: apiParameters.server + path,
            method: "POST",
            json: {
                email: req.body.email,
                password: req.body.password
            }
        };
        request(requestParameters, function(error, answer, token) {
            if(answer.statusCode == 200 && token.auth) {
                req.session.token = token.token;
                res.redirect("/login");
            } else if(answer.statusCode == 404) {
                res.redirect("/login?error=not-exists");
            } else {
                showError(req, res, answer.statusCode, token);
            }
        });
    } else {
        showError(req, res, 401);
    }
};
module.exports.registerForm = function(req, res) {
    if(req.session.token) {
        var redir = "/library";
        if(req.session.prevPage) {
            redir = req.session.prevPage;
            req.session.prevPage = undefined;
        }
        return res.redirect(redir);
    }
    
    var error = false;
    if(req.query && req.query.error !== undefined) {
        error = req.query.error;
    }
    res.render('user-register', {
        title: 'AA - Register',
        pageHeader: {
            title: 'Create account',
            subtitle: "For the best experience possible"
        },
        pageFooter: {
            left: [
                {
                    title: "Registration",
                    href: "/register"
                }
            ],
            right: ""
        },
        error: error
    });
};
module.exports.register = function(req, res) {
    if(req.body && req.body.name && req.body.email && req.body.password && req.body.password2 && req.body.terms) {
        if(req.body.password == req.body.password2) {
            if(req.body.terms == "on") {
                if(utils.isEmail(req.body.email)) {
                    var requestParameters, path, forwardedData;
                    path = '/api/v1/users';
                    
                    forwardedData = {
                        name: req.body.name,
                        email: req.body.email,
                        password: req.body.password
                    };
                    requestParameters = {
                        url: apiParameters.server + path,
                        method: 'POST',
                        json: forwardedData
                    };
                    request(
                        requestParameters,
                        function(error, answer, token) {
                            if(answer.statusCode == 201 && token.auth) {
                                req.session.token = token.token;
                                return res.redirect('/register/success');
                            } else if(answer.statusCode == 409) {
                                return res.redirect('/register?error=email-exists');
                            } else
                                return showError(req, res, answer.statusCode);
                        }
                    );
                } else
                    res.redirect('/register?error=email-not-valid');
            } else
                res.redirect('/register?error=terms');
        } else
            res.redirect('/register?error=passwords-not-match');
    } else
        res.redirect('/register?error'); 
};
module.exports.registerSuccessPage = function(req, res) {
    if(req.session.token) return res.redirect("/library");
    res.render('user-register-success', {
        title: 'AA - Register',
        pageHeader: {
            title: 'Registration successful',
            subtitle: "Thank you"
        },
        pageFooter: {
            left: [
                {
                    title: "Registration successful",
                    href: "/registerSuccess"
                }
            ],
            right: ""
        }
    });
};
module.exports.signout = function(req, res) {
    req.session.destroy(function(err) {
        if(err) return res.status(400).send({ message: "Error destroying session" });
        
        res.render('genericno-besedilo', {
            title: "Signed out",
            subtitle: "You dared use me!",
            content: "<p>You were successfully signed out.</p><p>We are sorry to see you go. Please come back soon, okay?</p>"
        });
    });
};

function showError(req, res, statusCode, error) {
    var title, content, subtitle;
    if(statusCode == 404) {
        title = "404";
        subtitle = "Page cannot be found";
        content = "Well, this is emberrasing";
    } else {
        title = statusCode + ", something went wrong";
        subtitle = "Something went wrong";
        content = "<p>Something could alwasy go wrong... And it just so happens that it did.</p><p>You could try dropping the database first.</p>";
    }
    
    res.render('genericno-besedilo', {
        title: title,
        subtitle: subtitle,
        content: content
    });
}
