var request = require('request');
request = request.defaults({
  headers: {'static-page': 'true'}
});

var apiParameters = {
  server: "http://localhost:" + process.env.PORT
};
if(process.env.NODE_ENV === 'production') {
  apiParameters.server = "https://aa-novels.herokuapp.com";
}

var Utils = require("./_include/utils");

/* Reviews */
module.exports.addReview = function(req, res) {
    getNovelDetails(req, res, function(req, res, novel) {
        displayAddReviewForm(req, res, novel);
    });
};
module.exports.saveReview = function(req, res) {
    var requestParameters, path, novel_id, forwardedData;
    novel_id = req.params.novel_id;
    path = '/api/v1/novels/' + novel_id + "/review";
    
    if(!req.body.rating) {
        res.redirect('/novel/' + novel_id + "/review?error=rating");
        return;
    }
    
    forwardedData = {
        token: req.session.token,
        rating: req.body.rating,
        review: req.body.review
    };
    requestParameters = {
        url: apiParameters.server + path,
        method: 'POST',
        json: forwardedData
    };
    request(
        requestParameters,
        function(error, answer, content) {
            if(answer.statusCode == 201) {
                res.redirect('/novel/' + novel_id);
            } else if (answer.statusCode == 400 && content.name && content.name === "ValidationError") {
                res.redirect('/novel/' + novel_id + "/review?error=values");
            } else {
                console.log("Error from saveReview");
                Utils.showError(req, res, answer.statusCode);
            }
        }
    );
};

/***** BEAN *****/
function displayAddReviewForm(req, res, novel) {
    res.render('novels-add-review', {
        title: 'AA - ' + novel.title + " - Review",
        pageHeader: {
            title: novel.title,
            subtitle: "Add review"
        },
        pageFooter: {
            left: [
                {
                    title: "Browse",
                    href: "/"
                },
                {
                    title: novel.title,
                    href: "/novel/" + novel._id
                },
                {
                    title: "Write a review",
                    href: ""
                }
            ],
            right: ""
        },
        error: req.query.error
    });
}
function getNovelDetails(req, res, callback) {
    var requestParameters, path;
    path = '/api/v1/novels';
    requestParameters = {
        url: apiParameters.server + path + "/" + req.params.novel_id,
        method: 'GET',
        json: {}
    };
    request(
        requestParameters,
        function(error, answer, novel) {
            if(answer.statusCode == 200) {
                callback(req, res, novel);
            } else {
                console.log("Error from getNovelDetails");
                Utils.showError(req, res, answer.statusCode);
            }
        }
    );
}
