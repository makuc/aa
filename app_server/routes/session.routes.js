var jwt = require("jsonwebtoken");

var Utils = require("../controllers/_include/utils");

module.exports.authentication = function(req, res, next) {
    var url = req.originalUrl;
    url = url.split(/[\/?#]/g);
    
    if(url[1] != "api" && !req.session.token) {
        return Utils.showError(req, res, 401);
    }
    next();
};
module.exports.session = function(req, res, next) {
    var url = req.originalUrl;
    url = url.split(/[\/?#]/g);
    
    if(url[1] != "api" && url[1] != "login" && url[1] != "register" && url[1] != "signout") {
        req.session.prevPage = req.originalUrl;
    }
    
    var token = req.query.token || req.headers['x-access-token'];
    if(req.body)
        token = req.body.token || token;
    
    if(token && req.session.token != token)
        req.session.token = token;
    
    if(req.session.token && !req.session.user) {
        return decodeToken(req, res, next);
    } else if(req.session.user) {
        console.log("UserID: " + req.session.user._id + " | Email: " + req.session.user.email + " | Url: " + req.originalUrl);
    } else if(url[1] != "api" && !token) {
        console.log("SessionID: " + req.sessionID + " | Url: " + req.originalUrl);
    }
    next();
};

function decodeToken(req, res, next) {
    jwt.verify(req.session.token, process.env.JWT_SECRET, function(err, decoded) {      
        if (err) {
            console.log("UserID: Guest with invalid token | SessionID: " + req.sessionID + " | Url: " + req.originalUrl);
            next();
        } else {
            // if everything is good, save to request for use in other routes
            req.session.user = decoded;
            console.log("UserID: " + decoded._id + " | Email: " + decoded.email + " | Url: " + req.originalUrl);
            
            next();
        }
    });
}