var express = require('express');
var router = express.Router();

var Novels = require("../controllers/novels.controllers");
var Reviews = require("../controllers/reviews.controllers");

// Different novel browsing
router.get('/library', Novels.library);
router.get('/mynovels', Novels.mynovels);

// Novels
router.get('/novel', Novels.addNovel);
router.post('/novel', Novels.addNovelSave);
//router.get('/novel/:novel_id/add', Novels.details);
//router.get('/novel/:novel_id/edit', Novels.editDetails);
router.post('/novel/:novel_id/library', Novels.addToLibrary);
router.post('/novel/:novel_id/library-remove', Novels.removeFromLibrary);
//router.post('/novel/:novel_id/edit', Novels.updateNovel);
//router.post('/novel/:novel_id/delete', Novels.deleteNovel);

// Chapters
//router.get('/novel/:novel_id/c', Novels.addChapter);
//router.post('/novel/:novel_id/c', Novels.addChapterSave);
//router.get('/novel/:novel_id/c/:chapter_id/edit', Novels.updateChapter);
//router.post('/novel/:novel_id/c/:chapter_id/edit', Novels.updateChapterSave);
//router.post('/novel/:novel_id/c/:chapter_id/delete', Novels.deleteChapter);

// Reviews
router.get('/novel/:novel_id/review', Reviews.addReview);
router.post('/novel/:novel_id/review', Reviews.saveReview);

module.exports = router;
