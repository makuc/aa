var express = require('express');
var router = express.Router();

var Users = require("../controllers/users.controllers");

//router.get('/register/success', Users.registerSuccessPage);
router.get('/signout', Users.signout);

module.exports = router;
