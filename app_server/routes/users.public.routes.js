var express = require('express');
var router = express.Router();

var Users = require("../controllers/users.controllers");

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/login', Users.loginForm);
router.post('/login', Users.login);
//router.get('/register', Users.registerForm);
//router.post('/register', Users.register);

module.exports = router;
