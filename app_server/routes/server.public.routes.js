var express = require('express');
var router = express.Router();

var Novels = require("../controllers/novels.controllers");
var Other = require("../controllers/other.controllers");
//var App = require("../controllers/app.controller");

// Novels pages
//router.get('/app', App.show);
router.get('/', Novels.browse);
router.get('/browse', Novels.browse);
router.get('/search', Novels.search);
router.get('/novel/:novel_id', Novels.details);

// Show chapters
router.get('/novel/:novel_id/c/:chapter_id', Novels.chapter);

// Ostale strani
router.get('/informacije', Other.informacije);
router.get('/db', Other.db);
router.get('/db/drop', Other.dbDrop);
router.get('/db/populate', Other.dbPopulate);

module.exports = router;
