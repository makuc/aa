if(process.env.NODE_ENV !== 'production')
  require('dotenv').load();

var express = require('express');
var app = express();
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
//var cookieParser = require('cookie-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var bodyParser = require('body-parser');
var config = require('./config.js');

// Needed for generating *.min.js client application
var uglifyJs = require('uglify-js');
var fs = require('fs');

require('./app_api/models/db');
var api = {
  public: require('./app_api/routes/api.public.routes'),
  private: require('./app_api/routes/api.private.routes'),
  authentication: require('./app_api/routes/api.authentication')
};

var server = {
  public: require("./app_server/routes/server.public.routes"),
  private: require("./app_server/routes/server.private.routes"),
  session: require("./app_server/routes/session.routes"),
};
var users = {
  public: require("./app_server/routes/users.public.routes"),
  private: require("./app_server/routes/users.private.routes")
};


// Files merging and minimizing --> *.min.js
var merged = uglifyJs.minify({
  'app.client.js': fs.readFileSync('app_client/app.client.js', 'utf8'),
  
  /* Controllers */
  'browse.constroller.js': fs.readFileSync('app_client/controllers/browse.controller.js', 'utf8'),
  'search.constroller.js': fs.readFileSync('app_client/controllers/search.controller.js', 'utf8'),
  'library.constroller.js': fs.readFileSync('app_client/controllers/library.controller.js', 'utf8'),
  'mynovels.constroller.js': fs.readFileSync('app_client/controllers/mynovels.controller.js', 'utf8'),
  'about.constroller.js': fs.readFileSync('app_client/controllers/about.controller.js', 'utf8'),
  'novel.constroller.js': fs.readFileSync('app_client/controllers/novel.controller.js', 'utf8'),
  'novelEdit.constroller.js': fs.readFileSync('app_client/controllers/novelEdit.controller.js', 'utf8'),
  'chapter.controller.js': fs.readFileSync('app_client/controllers/chapter.controller.js', 'utf8'),
  'chapterEdit.controller.js': fs.readFileSync('app_client/controllers/chapterEdit.controller.js', 'utf8'),
  'dbMng.constroller.js': fs.readFileSync('app_client/controllers/dbMng.controller.js', 'utf8'),
  '403.constroller.js': fs.readFileSync('app_client/controllers/403.controller.js', 'utf8'),
  //'library.constroller.js': fs.readFileSync('app_client/controllers/library.controller.js', 'utf8'),
  // User management
  'register.constroller.js': fs.readFileSync('app_client/controllers/auth/register.controller.js', 'utf8'),
  'login.constroller.js': fs.readFileSync('app_client/controllers/auth/login.controller.js', 'utf8'),
  // Modules controllers
  'modalReview.constroller.js': fs.readFileSync('app_client/modules/review/modalReview.controller.js', 'utf8'),
  
  /* Services */
  'data.service.js': fs.readFileSync('app_client/total/services/data.service.js', 'utf8'),
  'authentication.service.js': fs.readFileSync('app_client/total/services/authentication.service.js', 'utf8'),
  'dbMng.service.js': fs.readFileSync('app_client/total/services/dbMng.service.js', 'utf8'),
  
  /* Filters */
  'breakLineToHtml.filter.js': fs.readFileSync('app_client/total/filters/breakLineToHtml.filter.js', 'utf8'),
  'customDate.filter.js': fs.readFileSync('app_client/total/filters/customDate.filter.js', 'utf8'),
  
  /* Directives */
  'navigation.directive.js': fs.readFileSync('app_client/total/directives/navigation/navigation.directive.js', 'utf8'),
  'header.directive.js': fs.readFileSync('app_client/total/directives/header/header.directive.js', 'utf8'),
  'footer.directive.js': fs.readFileSync('app_client/total/directives/footer/footer.directive.js', 'utf8'),
  'noDefault.directive.js': fs.readFileSync('app_client/total/directives/noDefault/noDefault.directive.js', 'utf8'),
  'contenteditable.directive.js': fs.readFileSync('app_client/total/directives/contenteditable/contenteditable.directive.js', 'utf8'),
  'genres.directive.js': fs.readFileSync('app_client/total/directives/selectGenres/genres.directive.js', 'utf8'),
  'sidebar.directive.js': fs.readFileSync('app_client/total/directives/sidebar/sidebar.directive.js', 'utf8'),
  // Directives controllers
  'navigation.constroller.js': fs.readFileSync('app_client/total/directives/navigation/navigation.controller.js', 'utf8'),
  'genres.controller.js': fs.readFileSync('app_client/total/directives/selectGenres/genres.controller.js', 'utf8'),
  'sidebar.controller.js': fs.readFileSync('app_client/total/directives/sidebar/sidebar.controller.js', 'utf8'),
  
  
  'showRating.directive.js': fs.readFileSync('app_client/total/directives/showRating/showRating.directive.js', 'utf8')
});
fs.writeFile('public/angular/aa-novels.min.js', merged.code, function(err) {
  if (err)
    console.log(err);
  else
    console.log('Script is generated and saved in "public/angular/aa-novels.min.js"');
});

/*  Poveži se s podatkovno bazo  */
var dbURI = config.devDB;
if (process.env.NODE_ENV === 'production') {
    dbURI = process.env.MLAB_URI; // Bi bilo boljše nastaviti: config.prodDB ??
}

// Start session
var sess = {
  secret: process.env.JWT_SECRET,
  cookie: {},
  resave: true,
  saveUninitialized: true,
  store: new MongoStore({
    url: dbURI,
    ttl: 1 * 60 * 60, // = 1 hour in seconds. Default
    touchAfter: 5 * 60 // time period in seconds
  })
};

if (process.env.NODE_ENV === 'production') {
  app.set('trust proxy', 1); // trust first proxy
  sess.cookie.secure = true; // serve secure cookies
}

app.use(session(sess));

// view engine setup
app.set('views', path.join(__dirname, './app_server/views'));
app.set('view engine', 'pug');

// Set headers
app.use(function(req, res, next) {
  res.setHeader('X-Frame-Options', 'DENY');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
  res.setHeader('pragma', 'no-cache');
  next();
});


// Configure the rest of the file sharing
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

// Configure user logging
app.use(server.session.session);

// Serve app views
app.use(express.static(path.join(__dirname, 'app_client')));

// Serve static pages
app.use('/', server.public);
app.use('/', users.public);
app.use(server.session.authentication);
app.use('/', users.private);
app.use('/', server.private);

// Serve API
app.use(api.authentication.authenticate);
app.use('/api/v1', api.public);
// Checks for existance of a token
app.use(api.authentication.private);
// Private API from here
app.use('/api/v1', api.private);

/*
// Serve client
app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});
*/

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
